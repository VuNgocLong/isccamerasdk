//
//  ISCScanCameraViewController.swift
//  ISCCamera
//
//  Created by Long Vu on 4/11/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCScanCameraViewController: UIViewController {
    
    let ripple = ISCRipples()
    
    var arrDeviceCamera:[DeviceInfomation] = []
    
    @IBOutlet weak var rippleView: UIView!
    @IBOutlet weak var camFoundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ISCCameraSDK.shared().delegate = self
    }
    
    func runRipple() {
        ripple.radius = rippleView.bounds.size.width * 2/3
        ripple.rippleCount = 10
        ripple.backgroundColor = UIColor.blue.withAlphaComponent(0.7).cgColor
        rippleView.layer.addSublayer(ripple)
        ripple.start()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        ripple.position = rippleView.center
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        runRipple()
        ISCCameraSDK.shared().connectWLAN(self.ssid, password: "long0703")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ISCCameraSDK.shared().stopSearchCameraDevice()
    }

}

extension ISCScanCameraViewController:ISCCameraSDKDelegate {
    func errorFeedBack(_ error: String) {
        var message:String = error
        if error == ISC_ERROR_WLAN_INFO {
            message = "Invalid WIFI's name or Wifi's password!"
        }
        else if error == ISC_ERROR_WLAN_CONNECT {
            guard arrDeviceCamera.count == 0 else { return }
            message = "Cannot found camera device!"
        }
        let alertController = UIAlertController(title: "FPT Camera", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Accept", style: .default) { (_) in
            ISCCameraSDK.shared().stopSearchCameraDevice()
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func getCameraDeviceInfo(_ info: DeviceInfomation) {
        if !arrDeviceCamera.contains(where: { $0.deviceSerialNo != info.deviceSerialNo }) {
            arrDeviceCamera.append(info)
            let size:CGFloat = 30.0
            let offset:CGPoint = CGPoint(x:Int(arc4random()%1000),y:Int(arc4random()%1000))
            let image = UIImage(named: "ico-cam") as UIImage?
            let button   = UIButton(type: UIButton.ButtonType.custom) as UIButton
            button.frame = CGRect(x: offset.x, y: offset.y, width: size, height: size)
            button.setImage(image, for: .normal)
            button.tag = arrDeviceCamera.count-1
            camFoundView.addSubview(button)
            
            // TODO: Send API the device camera
            // Camera Data Scanned:
            print("Camera Name:\(info.deviceName ?? "IPC Camera"),Camera IP:\(info.deviceIp ?? ""),Camera SerialNo:\(info.deviceSerialNo ?? ""),Camera Mac:\(info.deviceMac ?? "")")
        }
    }
    @objc public func cameraDeviceTap(sender: UIButton) {
        print("Tap Camera ID:\(sender.tag)")
    }
}
