//
//  ISCLiveCameraController.swift
//  ISCCamera
//
//  Created by Long Vu on 4/5/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCLiveCameraController: UIViewController {

    @IBOutlet weak var livestreamView: UILivestreamView!
    @IBOutlet weak var qualityBtn :UIButton!
    @IBOutlet weak var muteBtn :UIButton!
    @IBOutlet weak var controlView: UIView!
    @IBOutlet weak var iconMic: UIImageView!
    
    var timerShowPauseBtn: Timer?
    
    var cameraID:String = ""
    var serialName:String = ""
    var loginName:String = ""
    var loginPsw:String = ""
    var name:String = ""
    
    var isAnimating:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.livestreamView.setup(self.serialName, loginName: self.loginName, loginPsw: self.loginPsw)
        self.livestreamView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
        self.controlView.tag = 10001
        self.setupControlView()
    }
    
    // TODO: Create subview menu for controlling playView
    fileprivate func setupControlView(){
        let height = CGFloat(64)
        self.view.addSubview(self.controlView)
        if #available(iOS 11.0, *) {
            NSLayoutConstraint.activate([
                self.controlView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
                self.controlView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
                self.controlView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,constant: -height),
                self.controlView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
                ])
        } else {
            NSLayoutConstraint.activate([
                self.controlView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                self.controlView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
                self.controlView.topAnchor.constraint(equalTo: self.view.bottomAnchor,constant: -height),
                self.controlView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
                ])
        }
    }
    
    // TODO: Need this to be 100% percent hide navigation bar in fullscreen.
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            self.view.layoutIfNeeded()
            self.view.viewWithTag(10001)?.removeFromSuperview()
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
        else {
            self.view.layoutIfNeeded()
            self.setupControlView()
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
    @objc func appDidEnterBackground() {
        // TODO: Need this code to pause talk manually
        if !self.livestreamView.isTalking {
            self.livestreamView.pauseTalk()
        }
        if timerShowPauseBtn != nil {
            timerShowPauseBtn?.invalidate()
            timerShowPauseBtn = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if timerShowPauseBtn != nil {
            timerShowPauseBtn?.invalidate()
            timerShowPauseBtn = nil
        }
        // TODO: If app presents or pushes new ViewController, this code maybe need
        self.livestreamView.startLivestream()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // TODO: If app presents or pushes new ViewController, this code maybe need
        self.livestreamView.playWillDisappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if timerShowPauseBtn != nil {
            timerShowPauseBtn?.invalidate()
            timerShowPauseBtn = nil
        }
        // TODO: If app presents or pushes new ViewController, this code maybe need
        self.livestreamView.playDidDisappear()
    }
    
    // TODO: Buttons Solution
    
    @IBAction func qualitySwitchTapped(_ sender: Any) {
        self.livestreamView.changeQualityLivestream()
    }
    
    @IBAction func fullscreenBtnTapped(_sender: Any) {
        self.livestreamView.fullScreenMode()
    }
    
    @IBAction func micTapped(_sender: Any) {
        if !self.livestreamView.isTalking {
            self.livestreamView.openTalk()
            self.iconMic.alpha = 1.0
            UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseInOut,.repeat,.autoreverse], animations: {
                self.iconMic.alpha = 0.0
            }) { (finished) in
            }
        }
        else {
            self.livestreamView.pauseTalk()
            UIView.animate(withDuration: 0.1, delay: 0.0, options: [.curveEaseInOut,.beginFromCurrentState], animations: {
                self.iconMic.alpha = 1.0
            }) { (finished) in
            }
        }
    }
    
    @IBAction func btnMutedTapped(_sender: Any) {
        self.livestreamView.setMutedLivestream()
    }
    
}

extension ISCLiveCameraController:UILivestreamViewDelegate {
    func successConnectCamera() {
        self.livestreamView.runLiveStream()
    }
    
    func loginFail(_ alert: UIAlertController) {
        self.present(alert, animated: true, completion: nil)
    }
    
    func changeQuality(_ quality: Bool) {
        self.qualityBtn.setTitle(quality ? "HD" : "SD" , for: .normal)
    }
    
    func setMuted(_ muted: Bool) {
        self.muteBtn.isSelected = !muted
    }
    
    func didTapScreen() {
        if timerShowPauseBtn != nil {
            timerShowPauseBtn?.invalidate()
            timerShowPauseBtn = nil
        }
        self.timerShowPauseBtn = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(runTimer), userInfo: nil, repeats: false)
      
        if DeviceInfo.Orientation.isLandscape {
            if let _ = self.view.viewWithTag(10001) {
                self.view.viewWithTag(10001)?.removeFromSuperview()
            }
            else {
                self.setupControlView()
            }
        }
    }
    
    func errorTalkback(_ error: String) {
        let alertController = UIAlertController(title: "FPT Camera", message: error, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Accept", style: .default) { (_) in }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func runTimer() {
        if DeviceInfo.Orientation.isLandscape {
            if let _ = self.view.viewWithTag(10001) {
                self.view.viewWithTag(10001)?.removeFromSuperview()
            }
        }
    }
}


