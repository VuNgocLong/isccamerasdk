//
//  ISCPlaybackCameraViewController.swift
//  ISCCamera
//
//  Created by Long Vu on 4/16/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCPlaybackCameraViewController: UIViewController {

    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var timerSlider: UISlider!
    @IBOutlet weak var endTimeLbl: UILabel!
    
    @IBOutlet weak var playView: UIPlaybackView!
    @IBOutlet weak var muteBtn :UIButton!
    @IBOutlet weak var controlView: UIView!
    @IBOutlet weak var iconPlay: UIImageView!
    
    var timerShowPauseBtn: Timer?
    
    var url:String = ""
    
    var isAnimating:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.playView.setup(url)
        self.playView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
    
        self.controlView.tag = 10002
        self.setupControlView()
    }
    
    // TODO: Create subview menu for controlling playView
    fileprivate func setupControlView(){
        let height = CGFloat(64)
        self.view.addSubview(self.controlView)
        if #available(iOS 11.0, *) {
            NSLayoutConstraint.activate([
                self.controlView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
                self.controlView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
                self.controlView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,constant: -height),
                self.controlView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
                ])
        } else {
            NSLayoutConstraint.activate([
                self.controlView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                self.controlView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
                self.controlView.topAnchor.constraint(equalTo: self.view.bottomAnchor,constant: -height),
                self.controlView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
                ])
        }
    }
    
    // TODO: Need this to be 100% percent hide navigation bar in fullscreen.
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            self.view.layoutIfNeeded()
            self.view.viewWithTag(10002)?.removeFromSuperview()
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
        else {
            self.view.layoutIfNeeded()
            self.setupControlView()
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
    @objc func appDidEnterBackground() {
        if timerShowPauseBtn != nil {
            timerShowPauseBtn?.invalidate()
            timerShowPauseBtn = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if timerShowPauseBtn != nil {
            timerShowPauseBtn?.invalidate()
            timerShowPauseBtn = nil
        }
        // TODO: If app presents or pushes new ViewController, this code maybe need
        self.playView.startPlayback()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if timerShowPauseBtn != nil {
            timerShowPauseBtn?.invalidate()
            timerShowPauseBtn = nil
        }
        // TODO: If app presents or pushes new ViewController, this code maybe need
        self.playView.playDidDisappear()
    }
    
    @IBAction func fullscreenBtnTapped(_sender: Any) {
        self.playView.fullScreenMode()
    }
    
    @IBAction func btnMutedTapped(_sender: Any) {
        self.playView.setMutedLivestream()
    }
    
    @IBAction func btnPauseResumeTapped(_sender: Any) {
        self.playView.pauseOrPlay()
    }
}

extension ISCPlaybackCameraViewController:UIPlaybackViewDelegate {
    
    func successConnectVideo() {
        print("Success Connect to Video")
    }
    
    func pauseVideo() {
        print("Pause Video")
        iconPlay.image = UIImage(named: "jc_play_normal.png")
    }
    
    func resumePlayVideo() {
        print("Play Video")
        iconPlay.image = UIImage(named: "jc_pause_normal.png")
    }
    
    func setMuted(_ muted: Bool) {
        self.muteBtn.isSelected = !muted
    }
    
    func getInfoPlayerInit(_ startTime: String, endTime: String, endTime endTimeInt: Int32) {
        self.startTimeLbl.text = startTime
        self.endTimeLbl.text = endTime
        self.timerSlider.maximumValue = Float(endTimeInt)
    }
    
    func getInfoPlayerRunning(_ playTime: String, playTime playTimeInt: Int32) {
        self.startTimeLbl.text = playTime
        self.timerSlider.setValue(Float(playTimeInt), animated: true)
    }
    
    func getInfoPlayerEnd(_ startTime: String) {
        self.startTimeLbl.text = startTime
        self.timerSlider.setValue(0, animated: true)
    }
    
    func didTapScreen() {
        if timerShowPauseBtn != nil {
            timerShowPauseBtn?.invalidate()
            timerShowPauseBtn = nil
        }
        self.timerShowPauseBtn = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(runTimer), userInfo: nil, repeats: false)
        
        if DeviceInfo.Orientation.isLandscape {
            if let _ = self.view.viewWithTag(10002) {
                self.view.viewWithTag(10002)?.removeFromSuperview()
            }
            else {
                self.setupControlView()
            }
        }
    }
    
    @objc func runTimer() {
        if DeviceInfo.Orientation.isLandscape {
            if let _ = self.view.viewWithTag(10002) {
                self.view.viewWithTag(10002)?.removeFromSuperview()
            }
        }
    }
    
}
