//
//  ISCViewController.swift
//  ISCCamera
//
//  Created by Long Vu on 4/2/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

class ISCViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func scanCamera(_ sender: Any) {
        let vc:ISCScanCameraViewController = self.storyboard?.instantiateViewController(withIdentifier: "ISCScanCameraViewController") as! ISCScanCameraViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func showLivestream(_ sender: Any) {
        let vc:ISCLiveCameraController = self.storyboard?.instantiateViewController(withIdentifier: "ISCLiveCameraController") as! ISCLiveCameraController
        vc.serialName = "0ca9e2fd91efa50d"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func playbackCamera(_ sender: Any) {
        let vc:ISCPlaybackCameraViewController = self.storyboard?.instantiateViewController(withIdentifier: "ISCPlaybackCameraViewController") as! ISCPlaybackCameraViewController
        vc.url = "http://42.117.7.84/playmp4/14.54.04-14.54.14[M][@762][0].h264"
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
