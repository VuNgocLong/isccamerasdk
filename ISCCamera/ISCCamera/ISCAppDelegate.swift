//
//  ISCAppDelegate.swift
//  ISCCamera
//
//  Created by Long Vu on 4/1/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

@objc class ISCAppDelegate: AppDelegate {
    @objc var enableAllOrientation = false
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        ISCCameraSDK.shared().initSDK()
        return true
    }
    override func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if (enableAllOrientation == true){
            return .allButUpsideDown
        }
        return .portrait
    }
}
