//
//  ISCCameraSDK.mm
//  ISCCameraSDK
//
//  Created by Long Vu on 4/1/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import "ISCCameraSDK.h"
#import "../ISCCameraSDK/Modules/ScanCamera/DeviceInfomation.h"

@implementation ISCCameraSDK

BOOL adb;
ScanCameraDevice *scanCamera;

static ISCCameraSDK *shareManager;
+ (ISCCameraSDK * )shared {
    return shareManager ? shareManager : [[ISCCameraSDK alloc]init];
}
- (id) init{
    static dispatch_once_t once;
    dispatch_once(&once ,^{
        shareManager = self;
    });
    self = shareManager;
    scanCamera = [ScanCameraDevice shared];
    scanCamera.delegate = self;
    return self;
}
// TODO: Init SDK Function
- (void)initSDK {
    [SDKInitializeModel SDKInit];
}
// TODO: Connect to WLAN to find Camera
- (void)connectWLAN:(NSString *)ssid password:(NSString *)password {
    [scanCamera searchCameraDevice:ssid password:password];
}
// TODO: Stop searching WLAN camera
- (void)stopSearchCameraDevice {
    [scanCamera stopSearch];
}

// TODO: Scan camera Delegate
// TODO: Error Feedback In Searching Camera
- (void)errorFeedBack:(NSString *)error {
    [self.delegate errorFeedBack:error];
}
// TODO: Callback Detect Camera Device
-(void)getCameraDeviceInfo:(DeviceInfomation *)info {
    [self.delegate getCameraDeviceInfo:info];
}

@end

