//
//  UIPlaybackView.m
//  ISCCamera
//
//  Created by Long Vu on 4/16/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import "UIPlaybackView.h"
#include <FunSDK/FunSDK.h>

@interface UIPlaybackView() {
    int _beginHour;
    int _beginMin;
    int _beginSec;
}

@end

@implementation UIPlaybackView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

-(void)dealloc {
    FUN_MediaStop(_myHandle);
    self.myHandle = 0;
    [self CloseHandle];
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

+(Class)layerClass {
    return [CAEAGLLayer class];
}

-(int)MsgHandle{
    if ( !_hObj ) {
        _hObj = FUN_RegWnd((__bridge void*)self);
    }
    return _hObj;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Playback

// TODO: Setup Playback View
-(void)setup:(NSString* )url {
    
    // Note: Data setup a camera play view
    
    self.playViewBound = self.bounds;
    self.bIsSound = NO;
    self.isPause = NO;
    self.isCompletedPlay = NO;
    
    self.url = url;
    
    [self setupGesutre];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotated:) name:UIDeviceOrientationDidChangeNotification object:nil];
}


//////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Gesture

-(void)setupGesutre {
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapAction:)];
    [singleTap setNumberOfTapsRequired:1];
    [self addGestureRecognizer:singleTap];
}

- (void)singleTapAction:(UIGestureRecognizer *)gestureRecoginizer {
    [self.delegate didTapScreen];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)rotated:(NSNotification *)notification {
    if ([JHRotatoUtil isOrientationLandscape]) {
        self->_lastOrientation = [UIApplication sharedApplication].statusBarOrientation;
        [self p_prepareFullScreen];
    }
    else {
        [self p_prepareSmallScreen];
    }
}

- (void)p_prepareFullScreen {
    UIViewController *topViewController = [self topViewController];
    [topViewController.navigationController setNavigationBarHidden:YES animated:YES];
}

// Chuyển sang chuẩn bị màn hình thu nhỏ
- (void)p_prepareSmallScreen {
    UIViewController *topViewController = [self topViewController];
    [topViewController.navigationController setNavigationBarHidden:NO animated:YES];
    [topViewController.navigationController.navigationBar setOpaque:0.4];
}


-(void)fullScreenMode {
    if (JHRotatoUtil.isOrientationLandscape) {
        [JHRotatoUtil forceOrientation:UIInterfaceOrientationPortrait];
    }
    else {
        [JHRotatoUtil forceOrientation:UIInterfaceOrientationLandscapeRight];
    }
}

-(void)setMutedLivestream {
    if (self.myHandle == 0) {
        self.bIsSound = NO;
        [self.delegate setMuted:self.bIsSound];
        return;
    }
    FUN_MediaSetSound(_myHandle, self.bIsSound ? 0 : 100);
    self.bIsSound = !self.bIsSound;
    [self.delegate setMuted:self.bIsSound];
}


// Note: Call this in View Did Appear
- (void)startPlayback {
    if (_myHandle==0) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Connecting......", nil)];
        _myHandle = Fun_MediaPlayByURL([self MsgHandle], [self.url UTF8String], (__bridge void *)self, 0);
    }
    ISCAppDelegate* appDelegate = (ISCAppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.enableAllOrientation = true;
}

// Note: Call this in View Did Disappear
- (void)playViewDidDisappear {
    FUN_MediaStop(_myHandle);
    _myHandle = 0;
    [self CloseHandle];
    [SVProgressHUD dismiss];
    
    ISCAppDelegate* appDelegate = (ISCAppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.enableAllOrientation = false;
}

-(void)CloseHandle{
    FUN_UnRegWnd(_hObj);
    _hObj = 0;
}

- (void)PauseOrPlay {
    if (self.isCompletedPlay) {
        self.isCompletedPlay = NO;
        [self startPlayback];
        return;
    }
    self.isPause = !self.isPause;
    FUN_MediaPause(_myHandle,-1);
}

- (void)OnFunSDKResult:(NSNumber*)pParam
{
    NSInteger nAddr = [pParam integerValue];
    MsgContent* msg = (MsgContent*)nAddr;
    
    switch (msg->id) {
        case EMSG_DEV_FIND_FILE: {
            [self.delegate successConnectVideo];
        } break;
        case EMSG_PAUSE_PLAY: {
            if (msg->param1 == 1) {
                [self.delegate resumePlayVideo];
            } else if (msg->param1 == 2) {
                [self.delegate pauseVideo];
            }
        } break;
        case EMSG_STOP_PLAY: {
            [SVProgressHUD dismiss];
            [self.delegate pauseVideo];
        } break;
        case EMSG_ON_PLAY_BUFFER_END: {
            if (!self.bIsSound) {
                FUN_MediaSetSound(_hObj, 100);
                self.bIsSound = !self.bIsSound;
            }
            if ([self.delegate respondsToSelector:@selector(endBufferStartPlaying)]) {
                [self.delegate endBufferStartPlaying];
            }
            [SVProgressHUD dismiss];
            
        } break;
        case EMSG_SEEK_TO_TIME: {
            if ([self.delegate respondsToSelector:@selector(seekTime)]) {
                [self.delegate seekTime];
            }
        }break;
        case EMSG_START_PLAY: {
            int endTime = msg->param3 - msg->param2 + 1;
            NSString* endStr = [NSString stringWithFormat:@"%@",[self changeSecToTimeText:endTime]];
            NSString* startStr = [NSString stringWithFormat:@"00:00"];
            [self.delegate getInfoPlayerInit:startStr endTime:endStr endTimeInt:endTime];
            [SVProgressHUD dismiss];
        } break;
        case EMSG_ON_PLAY_INFO: //收到解码信息回调
        {
            int playTime = msg->param2 - msg->param1;
            NSString* playStr = [self changeSecToTimeText:playTime];
            [self.delegate getInfoPlayerRunning:playStr playTimeInt:playTime];
        }
        break;
        case EMSG_ON_PLAY_END: {
            self.isPause = NO;
            self.isCompletedPlay = YES;
            NSString* startStr = [NSString stringWithFormat:@"00:00"];
            [self.delegate getInfoPlayerEnd:startStr];
            [self.delegate pauseVideo];
            
        }break;
        default: break;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Get TopViewController
- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)viewController {
    if ([viewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)viewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([viewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navContObj = (UINavigationController*)viewController;
        return [self topViewControllerWithRootViewController:navContObj.visibleViewController];
    } else if (viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed) {
        UIViewController* presentedViewController = viewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    }
    else {
        for (UIView *view in [viewController.view subviews])
        {
            id subViewController = [view nextResponder];
            if ( subViewController && [subViewController isKindOfClass:[UIViewController class]])
            {
                if ([(UIViewController *)subViewController presentedViewController]  && ![subViewController presentedViewController].isBeingDismissed) {
                    return [self topViewControllerWithRootViewController:[(UIViewController *)subViewController presentedViewController]];
                }
            }
        }
        return viewController;
    }
}

- (NSString *)changeSecToTimeText:(int)second
{
    NSString *timeStr = @"";
    int min = (int)second/60;
    NSString *minStr;
    if (min < 10) {
        minStr = [NSString stringWithFormat:@"0%d",min];
    }
    else{
        minStr = [NSString stringWithFormat:@"%d",min];
    }
    int sec = second- ((int)second/60) * 60;
    NSString *secStr;
    if (sec < 10) {
        secStr = [NSString stringWithFormat:@"0%d",sec];
    }
    else{
        secStr = [NSString stringWithFormat:@"%d",sec];
    }
    
    timeStr = [NSString stringWithFormat:@"%@:%@",minStr,secStr];
    return timeStr;
}

+ToNSStr:(const char*)szStr
{
    NSString* retStr;
    if (szStr) {
        retStr = [NSString stringWithUTF8String:szStr];
    }
    else {
        return @"";
    }
    
    if (retStr == nil || (retStr.length == 0 && strlen(szStr) > 0)) {
        NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
        NSData* data = [NSData dataWithBytes:szStr length:strlen(szStr)];
        retStr = [[NSString alloc] initWithData:data encoding:enc];
    }
    if (retStr == nil) {
        retStr = @"";
    }
    return retStr;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

@end
