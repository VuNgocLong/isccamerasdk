//
//  UIPlaybackView.h
//  ISCCamera
//
//  Created by Long Vu on 4/16/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import "Recode.h"
#import "GCDAsyncUdpSocket.h"
#import "JHRotatoUtil.h"

NS_ASSUME_NONNULL_BEGIN

// Protocol
@protocol UIPlaybackViewDelegate <NSObject>
// TODO: Must-have function callback
@required
// TODO: Callback when success connect video
-(void)successConnectVideo;
// TODO: Callback when pause video
-(void)pauseVideo;
// TODO: Callback when resume/play video
-(void)resumePlayVideo;
// TODO: Callback when set muted
-(void)setMuted:(BOOL)muted;
// TODO: Callback when tap screen
-(void)didTapScreen;
// TODO: Callback when player completed loading
-(void)getInfoPlayerInit:(NSString *)startTime endTime:(NSString *)endTime endTimeInt:(int)endTimeInt;
// TODO: Callback when player running
-(void)getInfoPlayerRunning:(NSString *)playTime playTimeInt:(int)playTimeInt;
// TODO: Callback when player end
-(void)getInfoPlayerEnd:(NSString *)startTime;
// TODO: Optional function callback
@optional
// TODO: Callback when end buffer and start playing video
-(void)endBufferStartPlaying;
// TODO: Callback when seek to time playing
-(void)seekTime;
@end


@interface UIPlaybackView : UIView
@property (nonatomic, assign) int myHandle;
@property (nonatomic) UIInterfaceOrientation lastOrientation;
@property (nonatomic, assign) CGRect playViewBound;
@property (readonly, nonatomic) int hObj;
@property (nonatomic) BOOL bIsSound;
@property (nonatomic) BOOL isPause;
@property (nonatomic) BOOL isCompletedPlay;


@property  (nonatomic,strong) NSString* url;

@property (nonatomic, assign) id <UIPlaybackViewDelegate> delegate;

// TODO: Setup Playback View
-(void)setup:(NSString* )url;
// TODO: Call this to start Playback
- (void)startPlayback;
// TODO: Call this to play Livestream when parent ViewController dismiss and reappear
- (void)playViewDidDisappear;
// TODO: Call this to pause or resume Playback
- (void)PauseOrPlay;
// TODO: Set Muted Livestream
-(void)setMutedLivestream;
// TODO: Full screen mode
-(void)fullScreenMode;

@end

NS_ASSUME_NONNULL_END
