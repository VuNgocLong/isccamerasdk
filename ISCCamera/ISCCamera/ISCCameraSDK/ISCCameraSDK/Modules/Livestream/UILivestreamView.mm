//
//  UILivestreamView.m
//  ISCCamera
//
//  Created by Long Vu on 4/4/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import "UILivestreamView.h"
#include <FunSDK/FunSDK.h>
#import "../../../Library/CoreData/SDKBaseFile/Common/Device/DeviceControl/DeviceControl.h"

@implementation UILivestreamView

// TODO: Setup PlayerView
TalkBackControl *talkControl;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

-(void)dealloc {
    [self CloseHandle];
    if (self.record) {
        [self.record stopRecode];
        //[_record release];
        self.record = nil;
    }
    [SVProgressHUD dismiss];
    self.myHandle = 0;
    if (self.isTalking) {
        [self pauseTalk];
    }
    talkControl = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

+(Class)layerClass {
    return [CAEAGLLayer class];
}

-(int)MsgHandle{
    if ( !_hObj ) {
        _hObj = FUN_RegWnd((__bridge void*)self);
    }
    return _hObj;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Camera

// TODO: Setup Camera Play View
-(void)setup:(NSString* )serialName loginName:(NSString* )loginName loginPsw: (NSString* )loginPsw {
    
    // Note: Data setup a camera play view
    
    self.playViewBound = self.bounds;
    self.isHighPixel = NO;
    self.bIsSound = NO;
    self.isSplitMode = NO;
    self.isTalking = NO;
    self.playStatus = PlayerStatusNone;
    
    self.serialName = serialName;
    self.loginName = loginName;
    self.loginPsw = loginPsw;
    
    talkControl = [[TalkBackControl alloc] init];
    talkControl.deviceMac = serialName;
    talkControl.channel = 0;
    talkControl.delegate = self;
    
    [self setupGesutre];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotated:) name:UIDeviceOrientationDidChangeNotification object:nil];
}


//////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Gesture

-(void)setupGesutre {
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapAction:)];
    [singleTap setNumberOfTapsRequired:1];
    [self addGestureRecognizer:singleTap];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanSwipe:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanSwipe:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self addGestureRecognizer:swiperight];
    
    UISwipeGestureRecognizer * swipeup=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanSwipe:)];
    swipeup.direction=UISwipeGestureRecognizerDirectionUp;
    [self addGestureRecognizer:swipeup];
    
    UISwipeGestureRecognizer * swipedown=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanSwipe:)];
    swipedown.direction=UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:swipedown];
    
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetected:)];
    [self addGestureRecognizer:pinchRecognizer];
}

- (void)singleTapAction:(UIGestureRecognizer *)gestureRecoginizer {
    [self.delegate didTapScreen];
}

- (void)handlePanSwipe:(UISwipeGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateChanged) {
        PTZ_ControlType ptz;
        switch (recognizer.direction) {
            case UISwipeGestureRecognizerDirectionUp:
                ptz = TILT_DOWN;
                break;
            case UISwipeGestureRecognizerDirectionDown:
                ptz = TILT_UP;
                break;
            case UISwipeGestureRecognizerDirectionLeft:
                ptz = PAN_RIGHT;
                break;
            case UISwipeGestureRecognizerDirectionRight:
                ptz = PAN_LEFT;
                break;
            default:
                ptz = TILT_UP;
                break;
        }
        FUN_DevPTZControl (_myHandle, [self.serialName UTF8String], 0, ptz, 0);
        sleep(1);
        FUN_DevPTZControl (_myHandle, [self.serialName UTF8String], 0, ptz, 1);
    }
}

- (void)pinchDetected:(UIPinchGestureRecognizer *)pinchRecognizer
{
    if (pinchRecognizer.state == UIGestureRecognizerStateEnded || pinchRecognizer.state == UIGestureRecognizerStateChanged) {
        CGFloat currentScale = self.frame.size.width / self.bounds.size.width;
        CGFloat newScale = currentScale * pinchRecognizer.scale;
        
        if (newScale < 1) {
            newScale = 1;
        }
        if (newScale > 3) {
            newScale = 3;
        }
        
        CGAffineTransform transform = CGAffineTransformMakeScale(newScale, newScale);
        self.transform = transform;
        pinchRecognizer.scale = 1;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)rotated:(NSNotification *)notification {
    if ([JHRotatoUtil isOrientationLandscape]) {
        self->_lastOrientation = [UIApplication sharedApplication].statusBarOrientation;
        [self p_prepareFullScreen];
    }
    else {
        [self p_prepareSmallScreen];
    }
}

- (void)p_prepareFullScreen {
    UIViewController *topViewController = [self topViewController];
    [topViewController.navigationController setNavigationBarHidden:YES animated:YES];
}

// Chuyển sang chuẩn bị màn hình thu nhỏ
- (void)p_prepareSmallScreen {
    UIViewController *topViewController = [self topViewController];
    [topViewController.navigationController setNavigationBarHidden:NO animated:YES];
    [topViewController.navigationController.navigationBar setOpaque:0.4];
}

// Note: Call this in View Did Appear
- (void)startLivestream {
    [self playCamera:self.serialName :self.loginName :self.loginPsw];
    if (self.playStatus == PlayerStatusStop) {
        self.playStatus = PlayerStatusNone;
    }
}

// Note: Call this in View Will Disappear
- (void)playViewWillDisappear {
    ISCAppDelegate* appDelegate = (ISCAppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.enableAllOrientation = false;
    
    if (self.playStatus == PlayerStatusPlayIng) {
        self.playStatus = PlayerStatusStop;
        FUN_MediaStop(self.myHandle);
        self.myHandle = 0;
    }
    if (self.isTalking) {
        [self pauseTalk];
    }
}

// Note: Call this in View Did Disappear
- (void)playViewDidDisappear {
    [self CloseHandle];
    if (self.record) {
        [self.record stopRecode];
        //[_record release];
        self.record = nil;
    }
    [SVProgressHUD dismiss];
    self.myHandle = 0;
    [self closeTalk];
    FUN_MediaStop(_myHandle);
    [self closeTalk];
}

-(void)CloseHandle{
    FUN_UnRegWnd(_hObj);
    _hObj = 0;
}

// TODO: Start Camera Live Stream
-(void)playCamera: (NSString* ) serialName : (NSString* ) loginName : (NSString* ) loginPsw{
    FUN_DevLogin([self MsgHandle], SZSTR(serialName), SZSTR(loginName), SZSTR(loginPsw), 0);
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Openning......", nil)];
    FUN_DevGetConfig_Json([self MsgHandle], SZSTR(serialName), "SystemInfo", 5000);
}

-(void)changeQualityLivestream {
    FUN_MediaStop(_myHandle);
    self.myHandle = FUN_MediaRealPlay(FUN_RegWnd((__bridge void*)self), [self.serialName UTF8String], 0, self.isHighPixel ? 1 : 0, (__bridge void*)self);
    self.isHighPixel = !self.isHighPixel;
    [self.delegate changeQuality:self.isHighPixel];
}

// TODO: Run Live Stream
-(void)runLiveStream {
    self.myHandle = FUN_MediaRealPlay(FUN_RegWnd((__bridge void*)self), [self.serialName UTF8String], 0, self.isHighPixel ? 0 : 1, (__bridge void*)self);
    self.playStatus = PlayerStatusPlayIng;
}

// TODO: Call Back SDK
- (void)OnFunSDKResult:(NSNumber*)pParam
{
    NSInteger nAddr = [pParam integerValue];
    MsgContent* msg = (MsgContent*)nAddr;
    
    switch (msg->id) {
        case EMSG_DEV_GET_CONFIG_JSON: {
            if (strcmp("Uart.PTZPreset", msg->szStr) == 0) {
                NSString *str = [UILivestreamView ToNSStr:msg->pObject];
                NSLog(@"Uart.PTZPreset %@",str);
            }
            if (strcmp("SystemInfo", msg->szStr) ==0) {
                NSString* pObject = [NSString stringWithUTF8String:msg -> pObject];
                if (pObject != nil){
                    NSString* serialNumber = [self getSerialNumber: pObject];
                    [self successGetConfigJSON_SerialNumber:serialNumber];
                }
            }
        }
            [self successLoginCameraConnection:msg->param1];
            break;
        case EMSG_START_PLAY: {
            if (msg->param1 == EE_OK) {
                [self startedPlaying];
            }
        } break;
        case EMSG_ON_PLAY_INFO:
        {
            [SVProgressHUD dismiss];
        } break;
        case EMSG_PAUSE_PLAY:
        {
            switch (self.playStatus) {
                case PlayerStatusNone:
                {}
                break;
                case PlayerStatusPlayIng:
                {
                    if ([self.delegate respondsToSelector:@selector(pausePlayer)]) {
                        [self.delegate pausePlayer];
                    }
                    self.playStatus = PlayerStatusPause;
                }
                break;
                case PlayerStatusPause:
                {
                    if ([self.delegate respondsToSelector:@selector(runPlayer)]) {
                        [self.delegate runPlayer];
                    }
                    self.playStatus = PlayerStatusPlayIng;
                }
                break;
                case PlayerStatusStop:
                {
                    if ([self.delegate respondsToSelector:@selector(stopPlayer)]) {
                        [self.delegate stopPlayer];
                    }
                    self.playStatus = PlayerStatusStop;
                }
                break;
                default:
                break;
            }
        }
        default: break;
    }
}

- (void)openTalk {
    talkControl.handle = [self MsgHandle];
    [talkControl startTalk];
    [self.delegate setMuted:YES];
    self.isTalking = YES;
}
- (void)pauseTalk {
    talkControl.handle = [self MsgHandle];
    [talkControl pauseTalk];
    [self.delegate setMuted:NO];
    self.isTalking = NO;
}
-(void)closeTalk {
    talkControl.handle = [self MsgHandle];
    [talkControl closeTalk];
    [self.delegate setMuted:NO];
    self.isTalking = NO;
}
-(void)errorTalkback:(NSString *)error {
    [self.delegate errorTalkback:error];
}


//////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Protocol Delegate
-(void)successGetConfigJSON_SerialNumber:(NSString *)serialNumber {
    if ([serialNumber isEqualToString:self.serialName]){
        [self.delegate successConnectCamera];
    }
}

-(void)successLoginCameraConnection:(int) param {
    if (param >= 0) {
        if ([self.delegate respondsToSelector:@selector(checkLoginCameraConnection:)]) {
            [self.delegate checkLoginCameraConnection:@"Successfully Access!"];
        }
        
    } else {
        if ([self.delegate respondsToSelector:@selector(checkLoginCameraConnection:)]) {
            [self.delegate checkLoginCameraConnection:@"Failure Access, please retry!"];
        }
        if (!_isSplitMode){
            [self tryToLoginAgain];
        }
    }
}

-(void)startedPlaying {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Connecting......", nil)];
    self.playStatus = PlayerStatusPlayIng;
    if ([self.delegate respondsToSelector:@selector(startedPlaying:)]) {
        [self.delegate startedPlaying:@"Launching LiveStream!"];
    }
    ISCAppDelegate* appDelegate = (ISCAppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.enableAllOrientation = true;
}

-(void)setMutedLivestream {
    if (self.myHandle == 0) {
        self.bIsSound = NO;
        [self.delegate setMuted:self.bIsSound];
        return;
    }
    FUN_MediaSetSound(_myHandle, self.bIsSound ? 0 : 100);
    self.bIsSound = !self.bIsSound;
    [self.delegate setMuted:self.bIsSound];
}

-(void)fullScreenMode {
    if (JHRotatoUtil.isOrientationLandscape) {
        [JHRotatoUtil forceOrientation:UIInterfaceOrientationPortrait];
    }
    else {
        [JHRotatoUtil forceOrientation:UIInterfaceOrientationLandscapeRight];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: Get TopViewController
- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)viewController {
    if ([viewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)viewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([viewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navContObj = (UINavigationController*)viewController;
        return [self topViewControllerWithRootViewController:navContObj.visibleViewController];
    } else if (viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed) {
        UIViewController* presentedViewController = viewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    }
    else {
        for (UIView *view in [viewController.view subviews])
        {
            id subViewController = [view nextResponder];
            if ( subViewController && [subViewController isKindOfClass:[UIViewController class]])
            {
                if ([(UIViewController *)subViewController presentedViewController]  && ![subViewController presentedViewController].isBeingDismissed) {
                    return [self topViewControllerWithRootViewController:[(UIViewController *)subViewController presentedViewController]];
                }
            }
        }
        return viewController;
    }
}

// TODO: Try login again
-(void)tryToLoginAgain{
    [SVProgressHUD dismiss];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"The account password is incorrect!" message:_serialName preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Password";
    }];
    UIAlertAction *cancelAct = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *okAct = [UIAlertAction actionWithTitle:@"Accept" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *pwd = alert.textFields.firstObject;
        self.loginPsw = pwd.text;
        [self playCamera: self.serialName : self.loginName : self.loginPsw];
    }];
    [alert addAction:cancelAct];
    [alert addAction:okAct];
    [self.delegate loginFail:alert];
    return;
}

+ToNSStr:(const char*)szStr
{
    NSString* retStr;
    if (szStr) {
        retStr = [NSString stringWithUTF8String:szStr];
    }
    else {
        return @"";
    }
    
    if (retStr == nil || (retStr.length == 0 && strlen(szStr) > 0)) {
        NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
        NSData* data = [NSData dataWithBytes:szStr length:strlen(szStr)];
        retStr = [[NSString alloc] initWithData:data encoding:enc];
    }
    if (retStr == nil) {
        retStr = @"";
    }
    return retStr;
}

-(NSString* )getSerialNumber: (NSString*) json{
    NSString* serialNumber = @"";
    NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    
    //    Note that JSONObjectWithData will return either an NSDictionary or an NSArray, depending whether your JSON string represents an a dictionary or an array.
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    if (error) {
        NSLog(@"Error parsing JSON: %@", error);
    }
    else
    {
        if ([jsonObject isKindOfClass:[NSArray class]])
        {
            NSLog(@"it is an array!");
            NSArray *jsonArray = (NSArray *)jsonObject;
            NSLog(@"jsonArray - %@",jsonArray);
        }
        else {
            NSLog(@"it is a dictionary");
            NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
            NSDictionary* systemInfo = [jsonDictionary objectForKey:@"SystemInfo"];
            serialNumber = [systemInfo objectForKey:@"SerialNo"];
        }
    }
    return serialNumber;
}



@end
