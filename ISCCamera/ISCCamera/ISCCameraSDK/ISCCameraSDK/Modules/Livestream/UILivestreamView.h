//
//  UILivestreamView.h
//  ISCCamera
//
//  Created by Long Vu on 4/4/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import "Recode.h"
#import "GCDAsyncUdpSocket.h"
#import "JHRotatoUtil.h"
#import "TalkControl/TalkBackControl.h"

NS_ASSUME_NONNULL_BEGIN

enum PlayStatus     // Trạng thái phát
{
    PlayerStatusNone,
    PlayerStatusPlayIng,
    PlayerStatusPause,
    PlayerStatusStop
};

enum TalkStatus
{
    TalkStatusNone,
    TalkStatusChange,
    TalkStatusTalking
};

enum VoiceStatus // Trạng thái giám sát âm thanh
{
    VoiceStatusNone,
    VoiceStatusPlaying
};


// Protocol
@protocol UILivestreamViewDelegate <NSObject>
// TODO: Must-have function callback
@required
// TODO: Callback when success connect Camera
-(void)successConnectCamera;
// TODO: Callback when login faile to Camera and return alert retry
-(void)loginFail:(UIAlertController *)alert;
// TODO: Callback when change quality
-(void)changeQuality:(BOOL)quality;
// TODO: Callback when set muted
-(void)setMuted:(BOOL)muted;
// TODO: Callback when tap screen
-(void)didTapScreen;
// TODO: Callback when error talk back
-(void)errorTalkback:(NSString *)error;
// TODO: Optional function callback
@optional
// TODO: Callback when check login Livestream
-(void)checkLoginCameraConnection:(NSString *)status;
// TODO: Callback when started Livestream
-(void)startedPlaying:(NSString *)status;
// TODO: Callback when pause Livestream
-(void)pausePlayer;
// TODO: Callback when run Livestream
-(void)runPlayer;
// TODO: Callback when stop Livestream
-(void)stopPlayer;
@end

@interface UILivestreamView : UIView<TalkBackControlDelegate>

@property  (nonatomic,strong) NSString* serialName;
@property  (nonatomic,strong) NSString* loginName;
@property  (nonatomic,strong) NSString* loginPsw;


@property (nonatomic, assign) int myHandle;

@property (nonatomic) UIInterfaceOrientation lastOrientation;
@property (nonatomic, assign) CGRect playViewBound;

@property (nonatomic, assign) enum PlayStatus playStatus;
@property (nonatomic, assign) enum TalkStatus talkStatus;
@property (nonatomic, assign) enum VoiceStatus voiceStatus;

@property (nonatomic, assign) Recode *record;

@property (readonly, nonatomic) int hObj;

@property (nonatomic) BOOL isHighPixel;
@property (nonatomic) BOOL bIsSound;
@property (nonatomic) BOOL isSplitMode;

@property (nonatomic, assign) id <UILivestreamViewDelegate> delegate;

@property (nonatomic) bool isTalking;

// TODO: Setup Camera Livestream
-(void)setup:(NSString* )serialName loginName:(NSString* )loginName loginPsw: (NSString* )loginPsw;
// TODO: Call this to start live stream
-(void)startLivestream;
// TODO: Call this to play Livestream when parent ViewController dismiss and reappear
-(void)playViewWillDisappear;
// TODO: Call this to play Livestream when parent ViewController dismiss and reappear
-(void)playViewDidDisappear;
// TODO: Run Livestream
-(void)runLiveStream;
// TODO: Change Quality Livestream
-(void)changeQualityLivestream;
// TODO: Set Muted Livestream
-(void)setMutedLivestream;
// TODO: Open Talk to Camera in Livestream
-(void)openTalk;
// TODO: Pause Talk to Camera in Livestream
-(void)pauseTalk;
// TODO: Close Talk to Camera in Livestream
-(void)closeTalk;
// TODO: Full screen mode
-(void)fullScreenMode;
@end

NS_ASSUME_NONNULL_END
