//
//  ScanCameraDevice.h
//  ISCCamera
//
//  Created by Long Vu on 4/10/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceInfomation.h"

NS_ASSUME_NONNULL_BEGIN

// Protocol
@protocol ScanCameraDeviceDelegate <NSObject>
@required
-(void)errorFeedBack:(NSString *)error;
-(void)getCameraDeviceInfo:(DeviceInfomation *)info;
@optional
@end

@interface ScanCameraDevice : NSObject
+ (ScanCameraDevice *) shared;
@property (readonly, nonatomic) int hObj;
@property (nonatomic, assign) id <ScanCameraDeviceDelegate> delegate;
- (void)searchCameraDevice:(NSString *)ssid password:(NSString *)password;
- (void)stopSearch;
@end

NS_ASSUME_NONNULL_END
