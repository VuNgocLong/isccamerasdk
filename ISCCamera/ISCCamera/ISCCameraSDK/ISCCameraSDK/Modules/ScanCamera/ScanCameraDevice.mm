//
//  ScanCameraDevice.m
//  ISCCamera
//
//  Created by Long Vu on 4/10/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import "ScanCameraDevice.h"
#include <FunSDK/FunSDK.h>
#import "getgateway.h"
#import "NSString+DealInternet.h"
#import "GUI.h"

@implementation ScanCameraDevice

NSTimer *_timer;
int _timeSearch;
BOOL _config;
BOOL _isSearching;

static ScanCameraDevice *shareManager;
+ (ScanCameraDevice * )shared {
    return shareManager ? shareManager : [[ScanCameraDevice alloc]init];
}
- (id) init{
    static dispatch_once_t once;
    dispatch_once(&once ,^{
        shareManager = self;
    });
    self = shareManager;
    _isSearching = NO;
    return self;
}

- (void)searchCameraDevice:(NSString *)ssid password:(NSString *)password {
    [self stopSearch];
    if (_timer != nil) {
        [_timer invalidate];
    }
    _timer = nil;
    if (ssid.length == 0 && password.length == 0) {
        [self.delegate errorFeedBack:ERROR_WLAN_INFO];
        return;
    }
    _isSearching = YES;
    _timeSearch = 120;
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countdown:) userInfo:nil repeats:YES];
    
    [self connectWLAN:ssid password:password];
}

-(void)connectWLAN:(NSString *)ssid password:(NSString *)password {
    in_addr_t addr = 0;
    getdefaultgateway(&addr);
    char szGetWay[64] = { 0 };
    IP2Str(addr, szGetWay);
    NSString* testip = [NSString getCurrent_IP_Address];
    char data[512] = { 0 };
    char infof[512] = { 0 };
    char szPwd[64] = { 0 };
    strncpy(szPwd, [password UTF8String], 63);
    strlen(szPwd);
    
    NSString* wifiConfigPath = [NSString getwifiConfiginfoPath];
    NSMutableArray* array =
    [NSMutableArray arrayWithContentsOfFile:wifiConfigPath];
    NSMutableArray* mutabarray = [[NSMutableArray alloc] init];
    if (array == nil) {
        array = [[NSMutableArray alloc] initWithCapacity:0];
    }
    for (int i = 0; i < array.count; i++) {
        [mutabarray addObject:[[array objectAtIndex:i] objectForKey:@"wifiname"]];
    }
    BOOL isExist = NO;
    for (int i = 0; i < mutabarray.count; i++) {
        if ([ssid isEqualToString:[mutabarray objectAtIndex:i]]) {
            NSDictionary* dic = [array objectAtIndex:i];
            [dic setValue:password forKey:@"wifipassword"];
            
            isExist = YES;
            break;
        }
    }
    if (isExist == NO) {
        NSMutableDictionary* dic = [NSMutableDictionary dictionary];
        
        [dic setObject:ssid forKey:@"wifiname"];
        [dic setObject:password forKey:@"wifipassword"];
        [array addObject:dic];
    }
    
    [array writeToFile:wifiConfigPath atomically:YES];
    
    sprintf(data, "S:%sP:%sT:%d", [ssid UTF8String], szPwd, 1);
    sprintf(infof, "gateway:%s ip:%s submask:%s dns1:%s dns2:%s mac:0", szGetWay, [testip UTF8String], "255.255.255.0", szGetWay, szGetWay);
    unsigned int mac[6];
    unsigned char mac2[6];
    NSString* macStr = [NSString getCurrent_Mac];
    sscanf([macStr UTF8String], "%x:%x:%x:%x:%x:%x", &mac[0], &mac[1], &mac[2],
           &mac[3], &mac[4], &mac[5]);
    for (int i = 0; i < 6; ++i) {
        mac2[i] = mac[i];
    }
    FUN_DevStartAPConfig([self MsgHandle], 3, [ssid UTF8String], data, infof, szGetWay, 1, 0, mac2, -1);
}

-(void)countdown:(NSTimer*)timer{
    if (_isSearching == NO ) {
        return;
    }
    _timeSearch = _timeSearch - 1;
    if (_timeSearch >= 0) {
        _config = YES;
    }else{
        _config = NO;
    }
    if (_timeSearch < 0) {
        [timer invalidate];
        [self.delegate errorFeedBack:ERROR_WLAN_CONNECT];
    }
}

-(void)CloseHandle{
    FUN_UnRegWnd(_hObj);
    _hObj = 0;
}

-(int)MsgHandle{
    if ( !_hObj ) {
        _hObj = FUN_RegWnd((__bridge void*)self);
    }
    return _hObj;
}

- (void)stopSearch
{
    FUN_DevStopAPConfig();
    if (_timer != nil) {
        [_timer invalidate];
    }
}

- (void)OnFunSDKResult:(NSNumber*)pParam {
    NSInteger nAddr = [pParam integerValue];
    MsgContent* msg = (MsgContent*)nAddr;
    switch (msg->id) {
        case EMSG_DEV_AP_CONFIG:
        case EMSG_DEV_SET_WIFI_CFG: {
            if (_timeSearch < 0) {
                return;
            }
            if (msg->id == EMSG_DEV_SET_WIFI_CFG && msg->param1 == 1) {
                break;
            }
            
            if (msg->param1 <= 0 || msg->pObject == NULL) {
                break;
            }
            [self detectCameraDevice:msg];
        } break;
        default:
            break;
    }
}

-(void)detectCameraDevice:(MsgContent *)msg {
    NSLog(@"Found Camera!");
    SDK_CONFIG_NET_COMMON_V2* pCfg = (SDK_CONFIG_NET_COMMON_V2*)msg->pObject;
    DeviceInfomation* deviceInfo = [[DeviceInfomation alloc] init];
    deviceInfo.deviceIp = [NSString stringWithFormat:@"%d.%d.%d.%d", pCfg->HostIP.c[0], pCfg->HostIP.c[1], pCfg->HostIP.c[2], pCfg->HostIP.c[3]];
    deviceInfo.deviceSerialNo = [NSString stringWithUTF8String:pCfg->sSn];
    
    deviceInfo.deviceMac = [NSString stringWithUTF8String:pCfg->sMac];
    deviceInfo.deviceName = [NSString stringWithUTF8String:pCfg->HostName];
    
    deviceInfo.devicePort = pCfg->TCPPort;
    deviceInfo.socketType = pCfg->DeviceType;
    [self.delegate getCameraDeviceInfo:deviceInfo];
}

@end
