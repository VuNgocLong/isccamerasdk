//
//  ISCCameraSDKConstant.swift
//  ISCCamera
//
//  Created by Long Vu on 4/11/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit

let ISC_ERROR_WLAN_INFO = "ERROR_WLAN_PASSWORD_OR_WLAN_NAME" // WIFI PASSWORD/NAME FAILED
let ISC_ERROR_WLAN_CONNECT = "ERROR_WLAN_CONNECTION_FAILED" // DETECT CAMERA FAILED
