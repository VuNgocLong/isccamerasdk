//
//  ISCCameraSDK.h
//  ISCCameraSDK
//
//  Created by Long Vu on 4/1/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDKInitializeModel.h"
#import "../ISCCameraSDK/Modules/ScanCamera/ScanCameraDevice.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ISCCameraSDKDelegate <NSObject>
@required
-(void)errorFeedBack:(NSString *)error;
-(void)getCameraDeviceInfo:(DeviceInfomation *)info;
@optional
@end

@interface ISCCameraSDK : NSObject<ScanCameraDeviceDelegate>

+ (ISCCameraSDK *) shared;
@property (nonatomic, assign) id <ISCCameraSDKDelegate> delegate;

- (void)initSDK;
- (void)connectWLAN:(NSString *)ssid password:(NSString *)password;
- (void)stopSearchCameraDevice;

@end


NS_ASSUME_NONNULL_END
