//
//  ISCExtension.swift
//  ISCCamera
//
//  Created by Long Vu on 4/8/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

import UIKit
import SystemConfiguration.CaptiveNetwork

// TODO: Get Device Rotate status
struct DeviceInfo {
    struct Orientation {
        // indicate current device is in the LandScape orientation
        static var isLandscape: Bool {
            get {
                return UIDevice.current.orientation.isValidInterfaceOrientation
                    ? UIDevice.current.orientation.isLandscape
                    : UIApplication.shared.statusBarOrientation.isLandscape
            }
        }
        // indicate current device is in the Portrait orientation
        static var isPortrait: Bool {
            get {
                return UIDevice.current.orientation.isValidInterfaceOrientation
                    ? UIDevice.current.orientation.isPortrait
                    : UIApplication.shared.statusBarOrientation.isPortrait
            }
        }
    }
}
// TODO: Get SSID - iOS 12: Must open WIFI Setting in Capability
extension UIViewController {
    var ssid : String {
        var currentSSID = ""
        if let interfaces = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces) {
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                if let interfaceData = unsafeInterfaceData as? [String: AnyObject] {
                    currentSSID = interfaceData["SSID"] as! String
                    debugPrint("ssid=\(currentSSID)")
                    break
                }
            }
        }
        return currentSSID
    }
}
