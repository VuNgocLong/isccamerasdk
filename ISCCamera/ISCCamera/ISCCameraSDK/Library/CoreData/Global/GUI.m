//
//  GUI.m
//  XMSmartHome
//
//  Created by mini14 on 2017/2/16.
//  Copyright © 2017年 jim. All rights reserved.
//

#import "GUI.h"

@implementation GUI
+ (char*)AutoCopyUTF8Str:(NSString*)string Unicode:(BOOL)unicode GBKcode:(BOOL)gbkcode
{
    return (char*)[string UTF8String];
    //    if ([self IsChinese:string]) {
    //#if ExperienceVersion
    //        return [GUI AutoCopyUTF8Str:string Unicode:unicode];
    //#else
    //        return [GUI getGBKWithString:string GBKcode:gbkcode];
    //#endif
    //    }
    //    else{
    //        return (char*)[string UTF8String];
    //    }
}

+ ToNSStr:(const char*)szStr
{
    NSString* retStr;
    if (szStr) {
        retStr = [NSString stringWithUTF8String:szStr];
    }
    else {
        return @"";
    }
    
    if (retStr == nil || (retStr.length == 0 && strlen(szStr) > 0)) {
        NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
        NSData* data = [NSData dataWithBytes:szStr length:strlen(szStr)];
        retStr = [[NSString alloc] initWithData:data encoding:enc];
    }
    if (retStr == nil) {
        retStr = @"";
    }
    return retStr;
}

@end
