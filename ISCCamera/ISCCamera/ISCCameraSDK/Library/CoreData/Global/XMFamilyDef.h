//
//  XMFamilyDef.h
//  XMFamily
//
//  Created by XM on 14-10-31.
//  Copyright (c) 2014年 XM. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum EITEMTYPE{
    EITEM_DEV          = 0,
    EITEM_SOCKET       = 1,   // 插座
    EITEM_BULB         = 2,   // 情景灯泡
    EITEM_BULB_SOCKET  = 3,   // 灯座
    EITEM_CAR          = 4,   // 汽车伴侣
    EITEM_BEYE         = 5,   // 大眼睛
    EITEM_SEYE         = 6,   // 小眼睛(小雨点)
    EITEM_ROBOT        = 7,   // 雄迈摇头机
    EITEM_SPORT_CAMERA = 8,   // 运动摄像机
    EITEM_FEYE         = 9,   // 鱼眼小雨点（小雨点全景摄像机）
    EITEM_FISH_BULB    = 10,  // 鱼眼灯泡（智能全景摄像灯泡）
    EITEM_BOB          = 11,  // 小黄人
    EITEM_MUSIC_BOX    = 12,  // wifi音乐盒
    EITEM_SPEAKER      = 13,  // wifi音响
    EITEM_RED_LINE     = 14,  // 智联中心
    EITEM_CHN,
    EITEM_EMPTY,
    
}EITEMTYPE;

typedef enum DEVICE_STATE
{
    STATE_UNKNOW,
    STATE_ONLINE,
    STATE_OFFLINE,
}DEVICE_STATE;

typedef enum OPERATION_STATE     // 操作状态
{
    OPERATION_STATE_NONE,     // 没有操作
    OPERATION_STATE_OPENING,  // 正在开
    OPERATION_STATE_CLOSING,  // 正在关
}OPERATION_STATE;

@interface TableItemInfo : NSObject

@property(readwrite, assign)DEVICE_STATE state;
@property(readwrite, assign)EITEMTYPE type;
@property(readwrite, assign)char *obj;
@property(readwrite, assign)int param1;
@property(readwrite, assign)int param2;
@property (nonatomic,assign) BOOL ifEdit;

// 插座相关
@property (nonatomic,strong) NSMutableDictionary *socketDic; //插座状态信息

// 智能灯泡相关
@property (readwrite,assign) BOOL ifOpen;
@property (readwrite,assign) OPERATION_STATE operation;     // 需要操作的状态

// 智能灯泡RGB值
@property (nonatomic,assign) BOOL enabled;  // 数据是否可用（有没有请求成功数据）
@property (readwrite,assign) int cR;
@property (readwrite,assign) int cG;
@property (readwrite,assign) int cB;
@property (readwrite,assign) int luma;

@end
