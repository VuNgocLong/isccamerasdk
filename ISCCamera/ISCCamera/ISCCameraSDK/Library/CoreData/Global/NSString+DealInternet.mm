//
//  NSString+DealInternet.m
//  XMFamily
//
//  Created by Megatron on 9/12/14.
//  Copyright (c) 2014 Megatron. All rights reserved.
//

#import "FunSDK/netsdk.h"
#import "NSString+DealInternet.h"
#import "LoginShowControl.h"
#import "XMFamilyDef.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import <SystemConfiguration/SystemConfiguration.h>
#include <arpa/inet.h>
#import <dlfcn.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <netdb.h>

#define shift(x) 1 << x
#define CURR_LANG ([[NSLocale preferredLanguages] objectAtIndex:0])
@implementation NSString (DealInternet)
+ (id)fetchSSIDInfo
{
    NSArray* ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    NSLog(@"Supported interfaces: %@", ifs);
    NSDictionary* infoDic = nil;
    for (NSString* ifnam in ifs) {
        infoDic = (__bridge NSDictionary*)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        NSLog(@"%@ => %@", ifnam, infoDic);
        if (infoDic && [infoDic count]) {
            break;
        }
    }
    return infoDic;
}

+ (NSString*)getCurrent_IP_Address
{
    NSString* address = @"error";
    struct ifaddrs* interfaces = NULL;
    struct ifaddrs* temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);

    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if (temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in*)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    freeifaddrs(interfaces);
    return address;
}

+ (NSString*)getCurrent_SSID;
{
    NSArray* ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    NSLog(@"Supported interfaces: %@", ifs);
    id infoDic = nil;
    for (NSString* ifnam in ifs) {
        infoDic = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        NSLog(@"%@ => %@", ifnam, infoDic);
        //if (infoDic && [infoDic count]) { break; }
    }
    const char* charSSID = [[infoDic objectForKey:@"SSID"] UTF8String];
    NSLog(@"%s", charSSID);
    if (TARGET_IPHONE_SIMULATOR) {
        charSSID = "kuozhanbu";
    }
    if (charSSID == NULL) {
        return @"";
    }
    NSString* ssid = [NSString stringWithUTF8String:charSSID];
    return ssid;
}
+ (NSString*)getCurrent_Mac
{
    NSArray* ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    NSLog(@"Supported interfaces: %@", ifs);
    id infoDic = nil;
    for (NSString* ifnam in ifs) {
        infoDic = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        NSLog(@"%@ => %@", ifnam, infoDic);
        //if (infoDic && [infoDic count]) { break; }
    }
    const char* charMac = [[infoDic objectForKey:@"BSSID"] UTF8String];
    NSLog(@"%s", charMac);
    if (TARGET_IPHONE_SIMULATOR) {
        charMac = "kuozhanbu";
    }
    if (charMac == NULL) {
        return @"";
    }
    NSString* Mac = [NSString stringWithUTF8String:charMac];
    return Mac;
}
+ (BOOL)checkSSID:(NSString*)ssid
{
    if ([ssid hasPrefix:@"robot_"] || [ssid hasPrefix:@"card"] || [ssid hasPrefix:@"car_"]
        || [ssid hasPrefix:@"seye_"] || [ssid hasPrefix:@"NVR"] || [ssid hasPrefix:@"DVR"]
        || [ssid hasPrefix:@"beye_"] || [ssid hasPrefix:@"IPC"] || [ssid hasPrefix:@"Car_"] || [ssid hasPrefix:@"BOB_"] || [ssid hasPrefix:@"socket_"] || [ssid hasPrefix:@"mov_"] || [ssid hasPrefix:@"xmjp_"] || [ssid hasPrefix:@"spt_"]) {
        return YES;
    }
    else {
        return NO;
    }
}
+ (int)getDeviceTypeWithSSID:(NSString*)ssid
{
    if (ssid == nil || ssid.length == 0)
        return -1;
    if ([ssid hasPrefix:@"robot_"] || [ssid hasPrefix:@"Robot_"]
        || [ssid hasPrefix:@"NVR_"] || [ssid hasPrefix:@"DVR_"]
        || [ssid hasPrefix:@"IPC_"] || [ssid hasPrefix:@"IPC"]) {
        return EITEM_DEV;
    }
    if ([ssid hasPrefix:@"socket_"] || [ssid hasPrefix:@"xmjp_socket_"]) {
        return EITEM_SOCKET;
    }
    if ([ssid hasPrefix:@"xmjp_bulb_"]) {
        return EITEM_BULB;
    }
    if ([ssid hasPrefix:@"xmjp_bulbsocket_"]) {
        return EITEM_BULB_SOCKET;
    }
    if ([ssid hasPrefix:@"car_"] || [ssid hasPrefix:@"xmjp_car_"]) {
        return EITEM_CAR;
    }
    if ([ssid hasPrefix:@"beye_"] || [ssid hasPrefix:@"xmjp_beye_"]) {
        return EITEM_BEYE;
    }
    if ([ssid hasPrefix:@"seye_"] || [ssid hasPrefix:@"xmjp_seye_"]) {
        return EITEM_SEYE;
    }
    if ([ssid hasPrefix:@"xmjp_robot_"]) {
        return EITEM_ROBOT;
    }
    if ([ssid hasPrefix:@"xmjp_mov_"] || [ssid hasPrefix:@"xmjp_spt_"]) {
        return EITEM_SPORT_CAMERA;
    }
    if ([ssid hasPrefix:@"feye_"] || [ssid hasPrefix:@"xmjp_feye_"]) {
        return EITEM_FEYE;
    }
    if ([ssid hasPrefix:@"xmjp_fbulb_"]) {
        return EITEM_FISH_BULB;
    }
    if ([ssid hasPrefix:@"xmjp_BOB_"]) {
        return EITEM_BOB;
    }
    if ([ssid hasPrefix:@"xmjp_musicbox_"]) {
        return EITEM_MUSIC_BOX;
    }
    if ([ssid hasPrefix:@"xmjp_speaker"]) {
        return EITEM_SPEAKER;
    }
    return EITEM_DEV;
}
+ (BOOL)chechSocketID:(NSString*)ssid
{
    if ([ssid hasPrefix:@"socket_"] || [ssid hasPrefix:@"xmjp_socket_"] || [ssid hasPrefix:@"xmjp_bulb"]) {
        return YES;
    }
    else {
        return NO;
    }
}
+ (BOOL)checkxmjp_mov
{
//    if ([[[LoginShowControl getInstance] getWifiSSID] hasPrefix:@"xmjp_mov"] || [[[LoginShowControl getInstance] getWifiSSID] hasPrefix:@"xmjp_spt"]) {
//        return YES;
//    }
//    else {
//        return NO;
//    }
    return NO;
}
+ (BOOL)checkxmjp
{
//    NSString* ssid = [[LoginShowControl getInstance] getWifiSSID];
//    if ([self checkSSID:ssid]) {
//        return YES;
//    }
//    else {
//        return NO;
//    }
    return NO;
}
+ (NSString*)getNetworkStatue
{
    UIApplication* app = [UIApplication sharedApplication];
    NSArray* subviews = [[[app valueForKey:@"statusBar"] valueForKey:@"foregroundView"] subviews];
    NSNumber* dataNetworkItemView = nil;

    for (id subview in subviews) {
        if ([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
            dataNetworkItemView = subview;
            break;
        }
    }

    NSNumber* num = [dataNetworkItemView valueForKey:@"dataNetworkType"];
    NSString* str;
    NSLog(@"%i", [num intValue]);
    switch ([num intValue]) {
    case 0:
        str = @"NONE";
        return str;
        break;
    case 1:
        str = @"2G";
        return str;
        break;
    case 2:
        str = @"3G";
        return str;
        break;
    case 5:
        str = @"WIFI";
        return str;
        break;
    default:
        str = @"Unknow";
        break;
    }
    str = @"Unknow";
    return str;
}
+ (BOOL)enableToConnectNetwork
{
    BOOL isconnect;
    Reachability* r = [Reachability reachabilityWithHostName:@"www.apple.com"];
    switch ([r currentReachabilityStatus]) {
    case NotReachable:
        isconnect = NO;
        break;
    case ReachableViaWiFi:
        isconnect = YES;
        break;
    case ReachableViaWWAN: {
        isconnect = YES;
    } break;
    default:
        break;
    }
    return isconnect;
}
//判断是否支持对讲
+ (BOOL)chechTalkStr:(NSString*)talkStr
{
    NSArray* array = [NSArray arrayWithObjects:@"07810", @"07510", @"07531", @"14840", @"07510", @"07531", @"12502", nil];
    if ([array indexOfObject:talkStr] == NSNotFound) {
        return NO;
    }
    else {
        return YES;
    }
}
/////////////
// 拿到历史记录的文件路径 记录序列号的历史
+ (NSString*)getHistoryPath
{
    NSArray* path = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* str = [path lastObject];
    NSString* user_History = [str stringByAppendingPathComponent:@"history.plist"];
    return user_History;
}

// 拿到保存的文件路径 记录是否自动登录,
+ (NSString*)getRememberPath
{
    NSArray* path = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* str = [path lastObject];
    NSString* user_History = [str stringByAppendingPathComponent:@"remember.plist"];
    return user_History;
}

// 拿到保存的文件路径 记录是否自动下载
+ (NSString*)getdownloadRememberPath
{
    NSArray* path = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* str = [path lastObject];
    NSString* user_History = [str stringByAppendingPathComponent:@"downloadremember.plist"];
    return user_History;
}
// 拿到wifi历史记录的文件路径
+ (NSString*)getWifiHistoryPath
{
    NSArray* path = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* str = [path lastObject];
    NSString* user_History = [str stringByAppendingPathComponent:@"wifi.plist"];
    return user_History;
}

+ (NSString*)getWifiIPHistoryPath
{
    NSArray* path = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* str = [path lastObject];
    NSString* wifi_ip = [str stringByAppendingPathComponent:@"wifi_ip.plist"];
    return wifi_ip;
}
//获取预设点文件路径
+ (NSString*)getPresetList
{
    NSArray* path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* str = [path lastObject];
    NSString* preset = [str stringByAppendingPathComponent:@"preset.plist"];
    return preset;
}

+ (NSString*)getLastLoginType
{
    NSArray* path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* str = [path lastObject];
    NSString* login = [str stringByAppendingPathComponent:@"login_type.plist"];
    return login;
}
+ (NSString*)getwifiConfiginfoPath
{
    NSArray* path = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* str = [path lastObject];
    NSString* user_History = [str stringByAppendingPathComponent:@"wifiConfigInfo.plist"];
    return user_History;
}

+ (NSString*)getAutoSnapPath
{
    NSArray* pathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);

    NSString* path = [pathArray objectAtIndex:0];
    NSString* ssid = [NSString getCurrent_SSID];
    path = [path stringByAppendingPathComponent:ssid];
    BOOL isDir;
    NSFileManager* manager = [NSFileManager defaultManager];
    BOOL isExist = [manager fileExistsAtPath:path isDirectory:&isDir];
    if (!(isDir && isExist)) {
        BOOL success = [manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
        if (success) {
            NSLog(@"create success!");
        }
    }
    return path;
}
//获取广告图片路径
+ (NSString*)getADImagePath
{
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    path = [path stringByAppendingPathComponent:@"/ADImages"];

    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:path]) {
        [manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return path;
}
// 获得保存报警截图图片的路径
+ (NSString*)getAlarmPicPath
{
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    path = [path stringByAppendingPathComponent:@"/TempImages"];

    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:path]) {
        [manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return path;
}
+ (NSString*)getNowDate
{
    NSDate* nowDate = [NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString* dateString = [dateFormatter stringFromDate:nowDate];
    return dateString;
}

//存储录像数组名字的文件地址
+ (NSString*)movieFilePath
{
    NSArray* filePathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* file = [filePathArray lastObject];
    NSString* filePath = [file stringByAppendingPathComponent:@"moive.plist"];
    return filePath;
}
//原始录像路径
+ (NSString*)OrigDownloadVideoPath
{
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* directoryStr = [path stringByAppendingString:@"/OriginalVideo"];
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:directoryStr]) {
    }
    else {
        NSError* error = nil;
        [manager createDirectoryAtPath:directoryStr withIntermediateDirectories:YES attributes:nil error:&error];
        if (!error) {
            [self getRecordMp4FilePath];
        }
    }
    return directoryStr;
}

//录像剪切和可播放录像的下载路径
+ (NSString*)getRecordMp4FilePath
{
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* directoryStr = [path stringByAppendingString:@"/RecordMp4"];
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:directoryStr]) {
    }
    else {
        [manager createDirectoryAtPath:directoryStr withIntermediateDirectories:YES attributes:nil error:nil];
        sleep(0.01);
        if ([manager fileExistsAtPath:directoryStr]) {
            NSDirectoryEnumerator* enumerator = [[NSFileManager defaultManager] enumeratorAtPath:path];
            for (NSString* fileName in enumerator) {
                //fileName 就是遍历到的每一个文件文件名;
                if ([fileName hasSuffix:@".mp4"]) {
                    NSString* filePath = [path stringByAppendingString:[NSString stringWithFormat:@"/%@", fileName]];
                    NSString* newPath = [directoryStr stringByAppendingString:[NSString stringWithFormat:@"/%@", fileName]];
                    NSError* error = nil;
                    [[NSFileManager defaultManager] moveItemAtPath:filePath toPath:newPath error:&error];
                    if (error) {
                        [[NSFileManager defaultManager] moveItemAtPath:filePath toPath:newPath error:&error];
                    }
                }
            }
        }
    }
    return directoryStr;
}

//获得视频和回放截图保存路径
+ (NSString*)getCaptureImagePath
{
    NSString* dateString = [NSString getSystemTimeString];
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* directoryStr = [path stringByAppendingString:@"/CaptureImage"];
    NSString* pictureFilePath = [directoryStr stringByAppendingFormat:@"/%@.jpg", dateString];
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:directoryStr]) {
    }
    else {
        [manager createDirectoryAtPath:directoryStr withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return pictureFilePath;
}
//运动相机缩略图
+ (NSString*)getSportThumbCapturePath
{
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* directoryStr = [path stringByAppendingString:@"/SportCaptureImage"];
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:directoryStr]) {
    }
    else {
        [manager createDirectoryAtPath:directoryStr withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return directoryStr;
}
//运动相机图片集中的缩略图
+ (NSString*)getSportFolderPressPath
{
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* directoryStr = [path stringByAppendingString:@"/SpFolderPressImage"];
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:directoryStr]) {
    }
    else {
        [manager createDirectoryAtPath:directoryStr withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return directoryStr;
}
//获得运动相机图片浏览下载原图文件夹路径
+ (NSString*)getSportDetailImagePath
{
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString* directoryStr = [path stringByAppendingString:@"/SportCaptureDetailImage"];
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:directoryStr]) {
    }
    else {
        [manager createDirectoryAtPath:directoryStr withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return directoryStr;
}
+ (NSString*)sportCaptureFileNameClear:(NSString*)fileName
{
    NSString* Name = [fileName stringByReplacingOccurrencesOfString:@"/" withString:@"~"];
    return Name;
}
//获取系统时间的时间字符串
int number = 0;
+ (NSString*)getSystemTimeString
{
    if (number < 98) {
        number++;
        if (number == 98) {
            number = 0;
        }
    }
    NSDate* nowDate = [NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH_mm_ss"];
    NSString* dateString = [dateFormatter stringFromDate:nowDate];
    return [dateString stringByAppendingString:[NSString stringWithFormat:@" %d", number]];
}
//根据软件版本号判断是否是直连小雨点
+ (BOOL)checkDirectlyLife:(NSString*)software
{
    if (software == nil) {
        return NO;
    }
    NSArray* array = [software componentsSeparatedByString:@"."];
    if (array != nil && [array count] < 7) {
        return NO;
    }
    else {
        NSString* softType = [array objectAtIndex:6];
        int soft = (int)[softType integerValue] / 10000;
//        if (soft == 1) {
//            [[LoginShowControl getInstance] setLoginDeviceType:DIRECT_RAIN_DROP];
//        }
        return NO;
    }
}
+ (long)GetFpsByNO:(int)resolution
{
    long fps[] = {
        704 * 576, //D1
        704 * 288, //HD1
        352 * 576, //BCIF
        352 * 288, //CIF
        176 * 144, //QCIF
        640 * 480, //VGA
        320 * 240, //QVGA
        480 * 480, //SVCD
        160 * 128, //QQVGA
        240 * 192, //ND1
        928 * 576, //650TVL
        1280 * 720, //720P
        1280 * 960, //1_3M
        1600 * 1200, //UXGA
        1920 * 1080, //1080P
        1920 * 1200, //WUXGA
        1872 * 1408, //2_5M
        2048 * 1536, //3M
        3744 * 1408, //5M
    };
    if (resolution >= 0 && resolution < N_RESOLUTION_COUNT) {
        return fps[resolution];
    }
    return 0;
}

+ (NSString*)GetResolutionByNO:(int)resolution
{
    //int nRes[] = {};
    NSString* str[] = { @"D1",
        @"HD1", ///< 352*576(PAL)	352*480(NTSC)
        @"BCIF", ///< 720*288(PAL)	720*240(NTSC)
        @"CIF", ///< 352*288(PAL)	352*240(NTSC)
        @"QCIF", ///< 176*144(PAL)	176*120(NTSC)
        @"VGA", ///< 640*480(PAL)	640*480(NTSC)
        @"QVGA", ///< 320*240(PAL)	320*240(NTSC)
        @"SVCD", ///< 480*480(PAL)	480*480(NTSC)
        @"QQVGA", ///< 160*128(PAL)	160*128(NTSC)
        @"ND1", ///< 240*192
        @"960H", ///< 926*576
        @"720P", ///< 1280*720
        @"960", ///< 1280*960
        @"UXGA ", ///< 1600*1200
        @"1080P", ///< 1920*1080
        @"WUXGA", ///< 1920*1200
        @"2_5M", ///< 1872*1408
        @"3M", ///< 2048*1536
        @"5M"
    };
    if (resolution >= 0 && resolution < N_RESOLUTION_COUNT) {
        return str[resolution];
    }
    return @"Unkown";
}
+ (NSInteger)getResolutionByName:(NSString*)resolName
{
    NSInteger x = -1;
    NSArray* array = [NSArray arrayWithObjects:
                                  @"D1",
                              @"HD1", ///< 352*576(PAL)	352*480(NTSC)
                              @"BCIF", ///< 720*288(PAL)	720*240(NTSC)
                              @"CIF", ///< 352*288(PAL)	352*240(NTSC)
                              @"QCIF", ///< 176*144(PAL)	176*120(NTSC)
                              @"VGA", ///< 640*480(PAL)	640*480(NTSC)
                              @"QVGA", ///< 320*240(PAL)	320*240(NTSC)
                              @"SVCD", ///< 480*480(PAL)	480*480(NTSC)
                              @"QQVGA", ///< 160*128(PAL)	160*128(NTSC)
                              @"ND1", ///< 240*192
                              @"650TVL", ///< 926*576
                              @"720P", ///< 1280*720
                              @"960", ///< 1280*960
                              @"UXGA ", ///< 1600*1200
                              @"1080P", ///< 1920*1080
                              @"WUXGA", ///< 1920*1200
                              @"2_5M", ///< 1872*1408
                              @"3M", ///< 2048*1536
                              @"5M", nil];
    for (int i = 0; i < [array count]; i++) {
        if ([resolName isEqualToString:[array objectAtIndex:i]]) {
            x = i;
        }
    }
    return x;
}
+ (int)getSubMaxMask:(int)resolution
{
//    enum SDK_CAPTURE_SIZE_t captureSize;
    int captureSize = 0;
    switch (resolution) {
    case SDK_CAPTURE_SIZE_D1: {
        captureSize = shift(SDK_CAPTURE_SIZE_D1) | shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_HD1: {
        captureSize = shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_BCIF: {
        captureSize = shift(SDK_CAPTURE_SIZE_BCIF) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_CIF: {
        captureSize = shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_QCIF: {
        captureSize = shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_VGA: {
        captureSize = shift(SDK_CAPTURE_SIZE_VGA) | shift(SDK_CAPTURE_SIZE_QVGA);
    } break;
    case SDK_CAPTURE_SIZE_QVGA: {
        captureSize = shift(SDK_CAPTURE_SIZE_QVGA);
    } break;
    case SDK_CAPTURE_SIZE_SVCD: {
        captureSize = shift(SDK_CAPTURE_SIZE_SVCD);
    } break;
    case SDK_CAPTURE_SIZE_QQVGA: {
        captureSize = shift(SDK_CAPTURE_SIZE_SVCD);
    } break;
    case SDK_CAPTURE_SIZE_ND1: {
        captureSize = shift(SDK_CAPTURE_SIZE_ND1);
    } break;
    case SDK_CAPTURE_SIZE_650TVL: {
        captureSize = shift(SDK_CAPTURE_SIZE_D1) | shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_720P: {
        captureSize = shift(SDK_CAPTURE_SIZE_D1) | shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_1_3M: {
        captureSize = shift(SDK_CAPTURE_SIZE_D1) | shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_UXGA: {
        captureSize = shift(SDK_CAPTURE_SIZE_D1) | shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_1080P: {
        captureSize = shift(SDK_CAPTURE_SIZE_D1) | shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_WUXGA: {
        captureSize = shift(SDK_CAPTURE_SIZE_D1) | shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_2_5M: {
        captureSize = shift(SDK_CAPTURE_SIZE_D1) | shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_3M: {
        captureSize = shift(SDK_CAPTURE_SIZE_D1) | shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    case SDK_CAPTURE_SIZE_5M: {
        captureSize = shift(SDK_CAPTURE_SIZE_D1) | shift(SDK_CAPTURE_SIZE_HD1) | shift(SDK_CAPTURE_SIZE_CIF) | shift(SDK_CAPTURE_SIZE_QCIF);
    } break;
    default:
        break;
    }
    return captureSize;
}

+ (BOOL)connectedToNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;

    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
    SCNetworkReachabilityFlags flags;

    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);

    if (!didRetrieveFlags) {
        return NO;
    }

    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    return (isReachable && !needsConnection) ? YES : NO;
}

+ (void)saveAlarmArray:(NSMutableArray*)array
{

    //保存本地化数据
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSData* encodeData = [NSKeyedArchiver archivedDataWithRootObject:array];
    [defaults setObject:encodeData forKey:@"AlarmDatas"];
    [defaults synchronize];
}

+ (NSMutableArray*)getAlarmArray
{
    //读出本地化数据
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSData* savedData = [defaults objectForKey:@"AlarmDatas"];
    if (savedData == nil) {
        return nil;
    }
    NSMutableArray* array = [NSMutableArray arrayWithArray:(NSArray*)[NSKeyedUnarchiver unarchiveObjectWithData:savedData]];
    NSLog(@"%d", (int)[array count]);
    return array;
}

+ (NSString*)setLocalizedString:(NSString*)key
{
    NSString* selectedL = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedLanguage"];
    if (selectedL == nil || [selectedL integerValue] == 0) {
        NSString* language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if ([language isEqual:@"en"]) {
            NSString* str = NSLocalizedString(key, nil);
            return str;
        }
        else if ([language isEqual:@"zh-Hans-CN"] || [language isEqual:@"zh-Hans"] || [language hasPrefix:@"zh-Hans"]) {
            NSString* path = [[NSBundle mainBundle] pathForResource:@"zh-Hans-CN" ofType:@"lproj"];
            if (path == nil) {
                path = [[NSBundle mainBundle] pathForResource:@"zh-Hans" ofType:@"lproj"];
            }
            NSBundle* languageBundle = [NSBundle bundleWithPath:path];
            NSString* str = [languageBundle localizedStringForKey:key value:@"" table:nil];
            return str;
        }
        else {
            NSString* path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
            NSBundle* languageBundle = [NSBundle bundleWithPath:path];
            NSString* str = [languageBundle localizedStringForKey:key value:@"" table:nil];
            return str;
        }
    }
    else if ([selectedL integerValue] == 1) {
        NSString* path = [[NSBundle mainBundle] pathForResource:@"zh-Hans-CN" ofType:@"lproj"];
        NSBundle* languageBundle = [NSBundle bundleWithPath:path];
        NSString* str = [languageBundle localizedStringForKey:key value:@"" table:nil];
        return str;
    }
    else {
        NSString* path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
        NSBundle* languageBundle = [NSBundle bundleWithPath:path];
        NSString* str = [languageBundle localizedStringForKey:key value:@"" table:nil];
        return str;
    }
}
+ (int)getStringLength:(NSString*)message
{
    int chinese = 0;
    for (int i = 0; i < [message length]; i++) {
        int a = [message characterAtIndex:i];
        if (a >= 0x4e00 && a <= 0x9fff)
            chinese++;
    }
    int length = (int)[message length] + chinese;
    return length;
}

+ (NSString*)toBinarySystemWithDecimalSystem:(NSString*)decimal
{
    int num = [decimal intValue];
    int remainder = 0; //余数
    int divisor = 0; //除数

    NSString* prepare = @"";

    while (true) {
        remainder = num % 2;
        divisor = num / 2;
        num = divisor;
        prepare = [prepare stringByAppendingFormat:@"%d", remainder];

        if (divisor == 0) {
            break;
        }
    }

    NSString* result = @"";
    for (int i = (int)prepare.length - 1; i >= 0; i--) {
        result = [result stringByAppendingFormat:@"%@",
                         [prepare substringWithRange:NSMakeRange(i, 1)]];
    }

    return result;
}

+ (NSMutableArray*)getStringNumberValid:(long)message
{
    if (message == 0) {
        return nil;
    }
    NSMutableArray* ValidArray = [[NSMutableArray alloc] init];
    NSString* binaryStr = [NSString toBinarySystemWithDecimalSystem:[NSString stringWithFormat:@"%ld", message]];
    for (NSInteger i = [binaryStr length]; i > 0; i--) {
        NSInteger j = message;
        NSInteger k;
        if (i > 0) {
            k = 1 << (i - 1);
        }
        else
            k = 1;
        if (j & k) {
            // 第i位为1
            [ValidArray addObject:[NSString stringWithFormat:@"%ld", (long)i]];
        }
    }
    return ValidArray;
}
@end
