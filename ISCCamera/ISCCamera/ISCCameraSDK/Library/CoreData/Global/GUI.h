//
//  GUI.h
//  XMSmartHome
//
//  Created by mini14 on 2017/2/16.
//  Copyright © 2017年 jim. All rights reserved.
//

#import <Foundation/Foundation.h>
#define Handle  [self MsgHandle]
@interface GUI : NSObject
+ (char*)AutoCopyUTF8Str:(NSString*)string Unicode:(BOOL)unicode GBKcode:(BOOL)gbkcode;
@end
