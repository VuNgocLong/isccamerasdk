//
//  XMFamilyDef.m
//  XMFamily
//
//  Created by XM on 14-10-31.
//  Copyright (c) 2014年 XM. All rights reserved.
//

#import "XMFamilyDef.h"

@implementation TableItemInfo

-(instancetype)init
{
    self = [super init];
    if (self) {
        self.cR = 0;
        self.cG = 0;
        self.cB = 0;
        self.luma = 1;
        self.ifOpen = NO;
        self.operation = OPERATION_STATE_NONE;
        self.ifEdit = NO;
        self.socketDic = [[NSMutableDictionary alloc] initWithCapacity:0];
        self.state = STATE_UNKNOW;
        self.enabled = NO;
    }
    
    return self;
}
@end
