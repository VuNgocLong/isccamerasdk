//
//  NSString+DealInternet.h
//  XMFamily
//
//  Created by Megatron on 9/12/14.
//  Copyright (c) 2014 Megatron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

#define N_RESOLUTION_COUNT 19

@interface NSString (DealInternet)

//获取当前wifi的SSID
+(id)fetchSSIDInfo;

// 获得当前网络连接的ip
+(NSString *)getCurrent_IP_Address;
// 获得当前wifi设备的ssid
+(NSString *)getCurrent_SSID;
+(NSString *)getCurrent_Mac;//获取当前wifi Mac地址
//  判断ssid
+(BOOL)checkSSID:(NSString*)ssid;
+(BOOL)chechSocketID:(NSString*)ssid;
+(BOOL)checkxmjp_mov;
+(BOOL)checkxmjp;          //判断是否是xmjp
//根据ssid判断当前连接的是什么设备
+(int)getDeviceTypeWithSSID:(NSString*)ssid;
// 获得当前网络状态 不需要联网
+(NSString *)getNetworkStatue;
//判断是否支持对讲
+(BOOL)chechTalkStr:(NSString*)talkStr;
///////////////////////////////////
// 拿到自动截图的路径
+(NSString *)getAutoSnapPath;
// 拿到历史记录的文件路径 记录序列号的历史
+(NSString *)getHistoryPath;
//获取预设点路径
+ (NSString*)getPresetList;
// 拿到保存的文件路径 记录是否自动登录
+(NSString *)getRememberPath;
+(NSString *)getdownloadRememberPath;
// 拿到wifi历史记录的文件路径
+(NSString *)getWifiHistoryPath;
// 拿到wifi ip连接 历史记录
+(NSString *)getWifiIPHistoryPath;
// 得到最后一次登录类型 记录的路径
+(NSString *)getLastLoginType;
// 获得配置设备wifi的ssid和密码的保存路径
+(NSString*)getwifiConfiginfoPath;
//获取广告图片路径
+(NSString *)getADImagePath;
// 获得保存报警截图图片的路径
+(NSString *)getAlarmPicPath;
//获得当前的日期
+(NSString *)getNowDate;
//存储录像数组名字的文件地址
+(NSString *)movieFilePath;
//原始录像路径
+(NSString *)OrigDownloadVideoPath;
//录像剪切和可播放录像下载路径
+(NSString *)getRecordMp4FilePath;
//获得视频和回放截图保存路径
+(NSString *)getCaptureImagePath;
//获得运动相机图片浏览缩略图路径
+(NSString*)getSportThumbCapturePath;
//获得运动相机图片集中的图片缩略图
+(NSString*)getSportFolderPressPath;
//获得运动相机图片浏览下载原图文件夹路径
+(NSString*)getSportDetailImagePath;
//运动相机图片名称处理
+(NSString*)sportCaptureFileNameClear:(NSString*)fileName;
//获取系统时间的时间字符串
+ (NSString *)getSystemTimeString;
///////////////
+(NSString *)GetResolutionByNO:(int)resolution;
//根据软件版本号判断是否是直连小雨点
+(BOOL)checkDirectlyLife:(NSString*)software;
+(long)GetFpsByNO:(int)resolution;

+(int)getSubMaxMask:(int)resolution;
+(NSInteger)getResolutionByName:(NSString *)resolName;

+(void)saveAlarmArray:(NSMutableArray*)array;
+(NSMutableArray*)getAlarmArray;
+ (NSString *)setLocalizedString:(NSString *)key;
+(BOOL)enableToConnectNetwork;
+(int)getStringLength:(NSString*)message;

//十进制转二进制
+ (NSString *)toBinarySystemWithDecimalSystem:(NSString *)decimal;
//判断当前二进制字符是否是1，1的时候有值
+(NSMutableArray*)getStringNumberValid:(long)message;
@end
