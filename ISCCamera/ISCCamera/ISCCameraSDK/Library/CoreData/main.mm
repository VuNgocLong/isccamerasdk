//
//  main.m
//  ISCCamera
//
//  Created by Long Vu on 4/1/19.
//  Copyright © 2019 fun.sdk.ftel.vn.su4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ISCCamera-Swift.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        signal(SIGPIPE, SIG_IGN);
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ISCAppDelegate class]));
    }
}
