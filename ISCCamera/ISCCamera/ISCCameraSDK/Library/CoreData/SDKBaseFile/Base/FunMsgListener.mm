//
//  FunMsgListener.mm
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "FunMsgListener.h"
#import "FunSDK/FunSDK.h"

@implementation FunMsgListener

-(instancetype)init{
    
    self.msgHandle = FUN_RegWnd((__bridge LP_WND_OBJ)self);
    return self;
    
}

-(void)dealloc{
    
    FUN_UnRegWnd(self.msgHandle);
    self.msgHandle = -1;
    
}

@end
