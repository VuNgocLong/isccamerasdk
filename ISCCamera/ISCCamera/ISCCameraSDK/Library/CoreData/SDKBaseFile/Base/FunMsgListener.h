//
//  FunMsgListener.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

/***
 SDK initialization class, these files are the parent class that must be used or inherited after calling most sdk interfaces.
 FunMsgListener The simplest interface uses an inherited class.
 After inheriting from this class, you can call the interface directly, except that you can get msgHandle and do nothing else.
 *****/

#import <Foundation/Foundation.h>

@interface FunMsgListener : NSObject

@property (nonatomic,assign) int msgHandle;

@end

@protocol FunSDKResultDelegate <NSObject>

@required
-(void)OnFunSDKResult:(NSNumber *)pParam;

@end
