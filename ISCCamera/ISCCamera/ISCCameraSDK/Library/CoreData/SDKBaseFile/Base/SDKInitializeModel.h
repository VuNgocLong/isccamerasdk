//
//  SDKInitializeModel.h
//  camera
//
//  Created by Phan Hai Son on 2019/02/13.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

/***
 SDK initialization class, these files are the parent class that must be used or inherited after calling most sdk interfaces.
 SDKInitializeModel  Initialize the class file of FunSDK
 *****/

#import <Foundation/Foundation.h>

@interface SDKInitializeModel : NSObject

// MARK: SDK initialization
+ (void)SDKInit;
@end
