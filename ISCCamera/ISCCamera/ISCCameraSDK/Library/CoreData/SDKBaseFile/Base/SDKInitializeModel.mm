//
//  SDKInitializeModel.mm
//  camera
//
//  Created by Phan Hai Son on 2019/02/13.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "SDKInitializeModel.h"
#import "FunSDK/FunSDK.h"

enum EDevListMode
{
    E_DevList_Local,  // Danh sách thiết bị cục bộ
    E_DevList_Server, // Danh sách thiết bị thông qua tài khoản máy chủ
    E_DevList_AP      // Chế độ AP, chỉ một thiết bị kết nối trực tiếp
};

@implementation SDKInitializeModel

+ (void)SDKInit {
    // 1, initialize the underlying library language and international language files
    [self initLanguage];
    // 2, initialize the app certificate, and cloud services
    [self initPlatform];
    // 3, initialize some necessary underlying configuration
    [self configParam];
    // 4, initialize
//    [self setDevListMode:E_DevList_Server];
//#if DEBUG
//    Fun_LogInit(FUN_RegWnd((__bridge void*)self), "", 0, "", LOG_UI_MSG);
//#endif
}

// MARK: 1, initialize the underlying library language and international language files
+ (void)initLanguage {
    //Initialize the underlying library language, the underlying library only supports Chinese and English
    SInitParam pa;
    pa.nAppType = H264_DVR_LOGIN_TYPE_MOBILE;
    strcpy(pa.sLanguage,"en");
    strcpy(pa.nSource, "xmshop");
    FUN_Init(0, &pa);
    
    //Initialize internationalized language files, app interface display language
    //Fun_InitLanguage([[[NSBundle mainBundle] pathForResource:language ofType:@"txt"] UTF8String]);
}

// MARK: 2, initialize the app certificate
+ (void)initPlatform {
    FUN_XMCloundPlatformInit(UUID, APPKEY, APPSECRET, MOVECARD);
}

// MARK: 3, initialize some necessary underlying configuration
+ (void)configParam {
    // Initialization related parameters must be set, the device information save path + file after successful account login
    FUN_SetFunStrAttr(EFUN_ATTR_SAVE_LOGIN_USER_INFO, SZSTR([NSString GetDocumentPathWith:@"UserInfo.db"]));
    
    // Local device password storage file, must be set
    FUN_SetFunStrAttr(EFUN_ATTR_USER_PWD_DB, SZSTR([NSString GetDocumentPathWith:@"password.txt"]));
    
    // Upgrade file storage path (just path, no file name)
    FUN_SetFunStrAttr(EFUN_ATTR_UPDATE_FILE_PATH, SZSTR([NSString GetDocumentPathWith:@""]));
    
    // Set whether the device upgrade file can be downloaded automatically,
    // 0 does not download automatically, automatically downloads under 1wifi, 2 automatically downloads when there is network
    FUN_SetFunIntAttr(EFUN_ATTR_AUTO_DL_UPGRADE, 0); //Which one to choose, you can change dynamically
    
    // Configuration file storage path (just the path, no file name)
    FUN_SetFunStrAttr(EFUN_ATTR_CONFIG_PATH, SZSTR([NSString GetDocumentPathWith:@"APPConfigs"]));
}

+ (void)setDevListMode:(int)mode {
    NSArray* pathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* path = [pathArray lastObject];
    
    if (E_DevList_Local == mode) {
        FUN_SysInit([[path stringByAppendingString:@"/LocalDevs.db"] UTF8String]);
    } else if (E_DevList_Server == mode) {
        FUN_SysInit("arsp.xmeye.net;arsp1.xmeye.net;arsp2.xmeye.net", 15010);
    } else if (E_DevList_AP == mode) {
        FUN_SysInitAsAPModel([[path stringByAppendingString:@"/APDevs.db"] UTF8String]);
    }

}


@end
