//
//  NSUserDefaultData.m
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "NSUserDefaultData.h"

@implementation NSUserDefaultData

//Whether to support automatic login
+ (BOOL)ifAutoLogin {
    return [[NSUserDefaults standardUserDefaults] boolForKey:AUTOLOGIN];
}
+ (void)autoLoginSave:(BOOL)value {
    [NSUserDefaultData boolValueSave:value key:AUTOLOGIN];
}

//Take out locally saved values
+ (BOOL)boolValueCheck:(NSString*)key {
    return [[NSUserDefaults standardUserDefaults]  boolForKey:key];
}
+ (NSString *)stringValueCheck:(NSString*)key {
    return [[NSUserDefaults standardUserDefaults]  objectForKey:key];
}
//Save to local
+ (void)boolValueSave:(NSString*)key {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:key];
}
+ (void)boolValueSave:(BOOL)value key:(NSString*)key {
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:key];
}
+ (void)stringValueSave:(NSString*)value Key:(NSString*)key {
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
}
+ (void)stringValueSave:(NSString*)key {
    [[NSUserDefaults standardUserDefaults] setObject:key forKey:key];
}
@end
