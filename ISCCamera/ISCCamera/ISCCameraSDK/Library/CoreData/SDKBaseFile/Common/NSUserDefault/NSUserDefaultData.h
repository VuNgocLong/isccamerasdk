//
//  NSUserDefaultData.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

//Whether to support automatic login
#define AUTOLOGIN @"Auto_Login"

////Is it the first search?
//#define FIRSTSEARCH @"first_search"
////Is it the first sort?
//#define RANKSECTION @"ranks"
//#define RANK @"rank"
////Automatic highlight switch
//#define LIGHTABLE @"lightAble"

#import <Foundation/Foundation.h>

@interface NSUserDefaultData : NSObject

//Whether to support automatic login
+ (BOOL)ifAutoLogin;
+ (void)autoLoginSave:(BOOL)value;

////Is it the first search sort?
//+(BOOL)ifFirstTimeSearchPromet;
//
////Is it the first sort?
//+(BOOL)ifFirstTimeRank;
//
////Is it automatically highlighted?
//+(BOOL)ifAutoLight;
//+(void)autoLightSave:(BOOL)value;


@end
