//
//  NSString+Path.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString * const KFisheyeMode = @"Fisheye_model";
NSString * const picturePlist = @"picture.plist";

@interface NSString (Path)

//Document Create a file directly in a folder
+ (NSString *)GetDocumentPathWith:(NSString *) fileName;
//Caches Create a file directly in a folder
+ (NSString *)GetCachesPathWith:(NSString *) fileName;

#pragma mark -- Saved image path plist
+ (NSString *)pictureFilePath;

#pragma mark - Fisheye mode saved path plist
+ (NSString *)fisheyeInfoFile;

#pragma mark - Whether to support the path of video correction save
+ (NSString *)correctInfoFile;

#pragma mark - Thumbnail path, including device thumbnails, app manual video thumbnails, pictures on the device, video thumbnails, and alarm history thumbnails
+ (NSString *)thumbnailPath;
#pragma mark - Alarm history picture
+ (NSString *)alarmMessagePicPath;
#pragma mark - Generate a device thumbnail file name
+ (NSString *)devThumbnailFile:(NSString*)devId andChannle:(int)channle;

#pragma mark - Store the path of the recording
+(NSString *)getVideoPath;
#pragma mark - The total path to save all images
+ (NSString *)getPhotoPath;

@end
