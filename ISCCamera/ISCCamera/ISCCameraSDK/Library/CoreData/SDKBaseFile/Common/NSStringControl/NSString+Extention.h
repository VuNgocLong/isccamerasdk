//
//  NSString+Extention.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extention)

#pragma mark - Pass in an integer representing time into a string of time format: 00:00
+ (NSString *)getTimeStringWihtNumber:(NSInteger)time;
#pragma mark - Convert decimal to binary, set to return NSString length
+ (NSString *)decimalTOBinary:(uint16_t)tmpid backLength:(int)length;
#pragma mark - Judging the time required by a binary octet string
+ (NSString *)getWeekTimeStringWithBinaryString:(int)num;
#pragma mark - Pass in an integer into a form of time, minute, and second
+ (NSString *)getDHMSStringWithIntNumber:(NSInteger)time;
#pragma mark - Dictionary with json
+ (NSString *)dictionaryToJson:(NSDictionary *)dic;
+ (NSString *)dictionaryToJsonWithoutWritingPrettyPrinted:(NSDictionary *)dic;//Turn without a newline symbol
#pragma mark - Check password format Password 8-32 digits must contain numbers and letters
+(BOOL)isValidatePassword:(NSString *)password;
#pragma mark - Detect mailbox format
+(BOOL)isValidateEmail:(NSString *)email;
#pragma mark - Check if the username is legal. 4-32 digits, consisting of Chinese/letters/numbers, but not pure numbers.
+(BOOL)isValidateUserName:(NSString *)userName;

@end
