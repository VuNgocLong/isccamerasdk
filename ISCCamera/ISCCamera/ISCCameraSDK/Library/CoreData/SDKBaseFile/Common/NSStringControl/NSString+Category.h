//
//  NSString+Category.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Category)
#pragma mark - International language translation
+ (NSString *)ToNSStr:(const char*)szStr;
#pragma mark - Whether to include a string
- (BOOL)isContainsString:(NSString *)sFind;
#pragma mark - String length
+ (int)countLengthWithString:(NSString *)str;

#pragma mark - String to NSData
+ (NSData *)AutoCopyUTF8Str:(NSString *)string;

#pragma mark - Get the SSID, IP, Wi-Fi name of the current network connection
+ (NSString *)getCurrent_SSID;
+ (NSString *)getCurrent_IP_Address;
+ (NSString *)getWifiName;
#pragma mark  - Determine whether it is a direct connected special device
+ (BOOL)checkSSID:(NSString *)ssid;

#pragma mark - Get time string
+ (NSString *)GetSystemTimeString;

#pragma mark - Read the pattern of fisheye
+ (int)fisheyeMode:(NSString *)devId;
#pragma mark - Save fisheye mode
+ (void)saveFisheye:(NSString *)devId mode:(int)fisheyeMode;

#pragma mark - Remove the current device to support correction
+(NSString*)getCorrectdev:(NSString*)devId;

#pragma mark - Get the device image string corresponding to the device type
+ (NSString *)getDeviceImageType:(int)type;

#pragma mark - Get device type string
+ (NSString *)getDeviceType:(int)type;

#pragma mark - Get the device image corresponding to the current device type
+ (NSString*)getDeviceImageNameWithType:(int)type;

#pragma mark - Get device network status
+ (NSString *)getDeviceNetType:(int)type;

#pragma mark - Get the current time string

@end
