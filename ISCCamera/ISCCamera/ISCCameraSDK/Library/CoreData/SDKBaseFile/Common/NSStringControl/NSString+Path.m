//
//  NSString+Path.m
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "NSString+Path.h"

@implementation NSString (Path)

#pragma mark - Fisheye mode saved path
+ (NSString *)fisheyeInfoFile {
    NSString *SSIDinfoFile = [[NSString configFilePath] stringByAppendingString:@"fisheyeInfo.plist"];
    return SSIDinfoFile;
}

#pragma mark - Whether to support the path of video correction save
+ (NSString *)correctInfoFile
{
    NSString *SSIDinfoFile = [[NSString configFilePath] stringByAppendingString:@"correctInfo.plist"];
    return SSIDinfoFile;
}

#pragma mark - Configuration file save path
+ (NSString *)configFilePath {
    NSString *configFilePath = [[NSString cachesPath] stringByAppendingString:@"/Configs/"];
    [NSString checkDirectoryExist:configFilePath];
    return configFilePath;
}

#pragma mark -- Saved image path
+ (NSString *)pictureFilePath {
    NSString *file = [self getPhotoPath];
    NSString *filePath = [file stringByAppendingPathComponent:picturePlist];
    return filePath;
}

#pragma mark - Generate a device thumbnail file name
+ (NSString *)devThumbnailFile:(NSString*)devId andChannle:(int)channle {
    NSString *devThumbnailFile = [[NSString devThumbnailPath] stringByAppendingFormat:@"/%@_%d.jpg",devId,channle];
    return devThumbnailFile;
}

#pragma mark - Device thumbnails in the device list
+ (NSString *)devThumbnailPath {
    NSString *devThumbnailPath = [[NSString thumbnailPath] stringByAppendingString:@"/Device"];
    [NSString checkDirectoryExist:devThumbnailPath];
    return devThumbnailPath;
}

#pragma mark - Thumbnail path, including device thumbnails, app manual video thumbnails, pictures on the device, video thumbnails, and alarm history thumbnails
+ (NSString *)thumbnailPath {
    NSString *thumbnailPath = [[NSString getPhotoPath] stringByAppendingString:@"/Thumbnail"];
    [NSString checkDirectoryExist:thumbnailPath];
    return thumbnailPath;
}

#pragma mark - Alarm history picture
+ (NSString *)alarmMessagePicPath {
    NSString *alarmMessagePicPath = [[NSString getPhotoPath] stringByAppendingString:@"/AlarmMessagePic"];
    [NSString checkDirectoryExist:alarmMessagePicPath];
    return alarmMessagePicPath;
}

#pragma mark - The total path to save all images
+ (NSString *)getPhotoPath {
    NSString *photosPath = [[NSString documentsPath] stringByAppendingString:@"/Photos"];
    [NSString checkDirectoryExist:photosPath];
    return photosPath;
}

#pragma mark - Store the path of the recording
+(NSString *)getVideoPath {
    NSString *file = [NSString cachesPath];
    return [NSString getVideoPathString:file];
}

+ (NSString *)getVideoPathString:(NSString *)file {
    file = [file stringByAppendingPathComponent:@"Video"];
    return [NSString checkDirectoryExist:file];
}

// NSDocument/fileName
+ (NSString *)GetDocumentPathWith:(NSString *) fileName {
    NSString* path = [NSString documentsPath];
    if (fileName != nil) {
        path = [path stringByAppendingString:@"/"];
        path = [path stringByAppendingString:fileName];
    }
    return path;
}
+ (NSString *)GetCachesPathWith:(NSString *) fileName {
    NSString *path = [NSString cachesPath];
    if (fileName != nil) {
        path = [path stringByAppendingString:@"/"];
        path = [path stringByAppendingString:fileName];
    }
    return path;
}
//Determine if the current folder exists
+(NSString*)checkDirectoryExist:(NSString*)file{
    NSFileManager *manager = [NSFileManager defaultManager];
    BOOL isDir;
    BOOL ifExist = [manager fileExistsAtPath:file isDirectory:&isDir];
    if (!(isDir && ifExist)) {
        BOOL create = [manager createDirectoryAtPath:file withIntermediateDirectories:YES attributes:nil error:nil];
        if (!create) {
        }
    }
    return file;
}
//NSDocument
+ (NSString *)documentsPath {
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [pathArray lastObject];
    return path;
}
+ (NSString *)cachesPath {
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [pathArray lastObject];
    return path;
}
@end
