//
//  LanguageManager.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#define TS(x)       [LanguageManager LanguageManager_TS:x]

#import <Foundation/Foundation.h>

@interface LanguageManager : NSObject
//Language internationalization
+ (NSString *)LanguageManager_TS:(const char*)key;
//Set default language
+ (void)setCurrentLanguage:(NSString *)language;
//Determine if the current language is English
+ (BOOL)checkSystemCurrentLanguageIsEnglish;
//Determine if the current language is Chinese
+ (BOOL)checkSystemCurrentLanguageIsSimplifiedChinese;
//Return the corresponding internationalization file according to the current system language
+ (NSString *)currentLanguage;
@end
