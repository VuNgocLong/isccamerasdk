//
//  LanguageManager.mm
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "LanguageManager.h"
#import <FunSDK/FunSDK.h>

@implementation LanguageManager

+ (NSString *)LanguageManager_TS:(const char*)key {
    const char *value;
    value = Fun_TS(key);
    return [self ToNSStr:value];
}
+ ToNSStr:(const char*)szStr {
    if (szStr == NULL) {
        NSLog(@"Error szStr is null!!!!!!!!!!");
        return @"";
    }
    NSString *retStr = [NSString stringWithUTF8String:szStr];
    if (retStr == nil || (retStr.length == 0 && strlen(szStr) > 0)) {
        NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
        NSData *data = [NSData dataWithBytes:szStr length:strlen(szStr)];
        retStr = [[NSString alloc] initWithData:data encoding:enc];
    }
    if (retStr == nil) {
        retStr = @"";
    }
    return retStr;
}

+ (void)setCurrentLanguage:(NSString *)language {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:language forKey:@"Language_String"];
}

//Determine if the current language is English
+ (BOOL)checkSystemCurrentLanguageIsEnglish{
    const NSString *englist = @"en";
    NSString *currentLanguage = [self currentLanguage];
    if ([englist isEqualToString:currentLanguage]) {
        return true;
    }else{
        return false;
    }
}

//Determine if the current language is Chinese
+ (BOOL)checkSystemCurrentLanguageIsSimplifiedChinese{
    const NSArray *languageTargs = @[@"zh_CN"];
    NSString *currentLanguage = [self currentLanguage];
    for (NSString *languageTarg in languageTargs) {
        if ([languageTarg isEqualToString:currentLanguage]) {
            return true;
        }
    }
    
    return false;
}

//Return the corresponding internationalization file according to the current system language
+ (NSString *)currentLanguage {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *languageStr = [userDefaults objectForKey:@"Language_String"];
    NSString *setLan = @"en";
    
    if (languageStr == nil || [languageStr isEqualToString:@"auto"]) {
        NSArray *languages = [NSLocale preferredLanguages];
        NSString *currentLanguage = [languages objectAtIndex:0];
        if ([currentLanguage isContainsString:@"zh-Hans"]) {
            setLan = @"zh_CN";
        }//else if ([currentLanguage isContainsString:@"zh-Hant"]) {
//            setLan = @"zh_TW";
//        }else if ([currentLanguage isContainsString:@"ko-"]) {
//            setLan = @"ko_KR";
//        }else if ([currentLanguage isContainsString:@"fr-"] || [currentLanguage isContainsString:@"fr-CA"]){
//            //Previous in French followed by Canadian French
//            setLan = @"fr";
//        }else if ([currentLanguage isContainsString:@"tr-"]) {
//            //Turkish language
//            setLan = @"tr_TR";
//        }else if ([currentLanguage isContainsString:@"ru-"]) {
//            //Russian
//            setLan = @"ru";
//        }else if ([currentLanguage isContainsString:@"pt-PT"] || [currentLanguage isContainsString:@"pt-BR"]) {
//            //Front for Portugal followed by Brazil (Portuguese)
//            setLan = @"pt";
//        }else if ([currentLanguage isContainsString:@"it-"]) {
//            //Italian
//            setLan = @"ita";
//        }else if ([currentLanguage isContainsString:@"es-"] || [currentLanguage isContainsString:@"es-MX"] || [currentLanguage isContainsString:@"es-419"]) {
//            //Front for Spain, back for Brazil, last for Latin America (Spain)
//            setLan = @"es";
//        }else if ([currentLanguage isContainsString:@"de-"]) {
//            //German
//            setLan = @"ge";
//        }
    }else if ([languageStr isEqualToString:@"english"]){
        setLan = @"en";
    }else if ([languageStr isEqualToString:@"zh_cn"]) {
        setLan = @"zh_CN";
    }//else if ([languageStr isEqualToString:@"zh_tw"]){
//        setLan = @"zh_TW";
//    }else if ([languageStr isEqualToString:@"ko_kr"]){
//        setLan = @"ko_KR";
//    }else if ([languageStr isEqualToString:@"fr"]){
//        setLan = @"fr";//French
//    }else if ([languageStr isEqualToString:@"tr_TR"]){
//        setLan = @"tr_TR";//Turkish
//    }else if ([languageStr isEqualToString:@"ru"]){
//        setLan = @"ru";//Russian
//    }else if ([languageStr isEqualToString:@"pt"]){
//        setLan = @"pt";//Portuguese
//    }else if ([languageStr isEqualToString:@"ita"]){
//        setLan = @"ita";//Italian
//    }else if ([languageStr isEqualToString:@"es"]){
//        setLan = @"es";//Spanish
//    }else if ([languageStr isEqualToString:@"de"]){
//        setLan = @"ge";//German
//    }
    return setLan;
    
}
@end
