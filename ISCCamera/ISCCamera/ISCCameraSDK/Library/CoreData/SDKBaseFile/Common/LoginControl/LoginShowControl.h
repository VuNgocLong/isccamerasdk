//
//  LoginShowControl.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

/***
 
Login information control
 
 *****/

//Login mode
typedef enum {
    loginTypeNone = 0,
    loginTypeCloud,//Cloud landing
    loginTypeLocal,//Local login
    loginTypeAP, //Ap mode
    loginTypeWX,//WeChat
} LoginType;

#import <Foundation/Foundation.h>

@interface LoginShowControl : NSObject

+ (instancetype)getInstance;

#pragma mark - Automatic login switch
+ (void)setAutoLoginType:(BOOL)loginType;
+ (BOOL)getAutoLoginType;

#pragma mark - Login mode
- (void)setLoginType:(int)loginType;
- (int)getLoginType;

#pragma mark - Login account and password
- (void)setLoginUserName:(NSString *)user password:(NSString *)psw;
- (NSString *)getLoginUserName;
- (NSString *)getLoginPassword;

#pragma mark - Whether the app push function is turned on
- (void)setPushFunction:(BOOL)open;
- (BOOL)getPushFunction;

#pragma mark - Push token
- (void)setPushToken:(NSString *)token;
- (NSString *)getPushToken;

@end
