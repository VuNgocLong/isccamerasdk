//
//  LoginShowControl.m
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "LoginShowControl.h"

@implementation LoginShowControl

+ (instancetype)getInstance {
    static dispatch_once_t onceToken;
    static LoginShowControl *instance;
    dispatch_once(&onceToken, ^{
        instance = [[LoginShowControl alloc] init];
    });
    return instance;
}
- (id)init {
    self = [super init];
    return self;
}

#pragma mark - Automatic login switch
+ (void)setAutoLoginType:(BOOL)loginType {
    [NSUserDefaultData autoLoginSave:loginType];
}
+ (BOOL)getAutoLoginType {
    return [NSUserDefaultData ifAutoLogin];
}

#pragma mark - Login mode, including account login, local login, ap login
LoginType appLoginType = loginTypeNone;
- (void)setLoginType:(int)loginType {
    appLoginType = loginType;
}
- (int)getLoginType {
    return appLoginType;
}

#pragma mark - Login account and password
NSString * userName = @""; NSString *password = @"";
- (void)setLoginUserName:(NSString*)user password:(NSString*)psw {
    userName = user; password = psw;
}
- (NSString *)getLoginUserName {
    return userName;
}
- (NSString *)getLoginPassword {
    return password;
}

#pragma mark - Whether the app push function is turned on
BOOL pushFunction = NO;
- (void)setPushFunction:(BOOL)open {
    pushFunction = open;
}
- (BOOL)getPushFunction {
    return pushFunction;
}

#pragma mark - Push token
NSString *tokenStr = @"";
- (void)setPushToken:(NSString *)token {
    tokenStr = token;
}
- (NSString *)getPushToken {
    return tokenStr;
}

@end
