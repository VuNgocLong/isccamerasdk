//
//  DeviceControl.m
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "DeviceControl.h"
#import "DevicelistArchiveModel.h"

@interface DeviceControl ()
{
    NSMutableArray *deviceArray; //Array containing all devices
    NSMutableArray *channelPlayingArray; //The array of device channels to be played
    NSInteger selectChannel; //The index of the channel being processed in the current preview channel array (effective for multi-channel preview, but defaults to 0 when previewing the channel)
}
@end

@implementation DeviceControl

+ (instancetype)getInstance {
    static DeviceControl *Manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Manager = [[DeviceControl alloc]init];
    });
    return Manager;
}

- (id)init {
    self = [super init];
    deviceArray = [[NSMutableArray alloc] initWithCapacity:0];
    channelPlayingArray = [[NSMutableArray alloc] initWithCapacity:0];
    [DevicelistArchiveModel sharedDeviceListArchiveModel];
    return self;
}

#pragma mark - Empty all cached devices
- (void)clearDeviceArray {
    [deviceArray removeAllObjects];
}

#pragma mark - Add device
- (void)addDevice:(DeviceObject*)devObject {
    [deviceArray addObject:devObject];
}

#pragma mark - Get all devices
- (NSMutableArray*)currentDeviceArray {
    return deviceArray;
}

#pragma mark - Device channel array processing to be previewed
- (void)setPlayItem:(ChannelObject *)channel {
    [channelPlayingArray addObject:channel];
}

- (NSMutableArray *)getPlayItem {
    return [channelPlayingArray mutableCopy];
}

- (void)cleanPlayitem {
    [channelPlayingArray removeAllObjects];
}

#pragma mark - Set the channel currently being processed, such as the channel in the capture video, the channel in the device configuration, etc.
- (void)setSelectChannel:(NSInteger)selectChannel {
    selectChannel = selectChannel;
}

- (NSInteger)getSelectChannelIndex {
    return selectChannel;
}

- (ChannelObject*)getSelectChannel {
    return [channelPlayingArray objectAtIndex:selectChannel];
}

#pragma mark - Save the device to local storage (restore the device data when it changes)
- (void)saveDeviceList {
    [[DevicelistArchiveModel sharedDeviceListArchiveModel] saveDevicelist:[deviceArray mutableCopy]];
}

#pragma mark - Get the deviceObject object by serial number
- (DeviceObject *)GetDeviceObjectBySN:(NSString *)devSN {
    if (deviceArray == nil || devSN == nil) {
        return nil;
    }
    for (int i = 0; i < deviceArray.count; i ++) {
        DeviceObject *devObject = [deviceArray objectAtIndex:i];
        if (devObject == nil) {
            continue;
        }
        if ([devObject.deviceMac isEqualToString:devSN]) {
            return devObject;
        }
    }
    return nil;
}

#pragma mark - Get the channelObject object
- (ChannelObject*)addName:(NSString*)channelName ToDeviceObject:(DeviceObject*)devObject {
    ChannelObject *object = [[ChannelObject alloc] init];
    object.channelName = channelName;
    object.deviceMac = devObject.deviceMac;
    object.loginName = devObject.loginName;
    object.loginPsw = devObject.loginPsw;
    return object;
}

#pragma mark - Device list comparison (after obtaining the device list successfully, the device obtained by the server is compared with the locally cached device)
- (void)checkDeviceValid {
    //Pass the array
    [[DevicelistArchiveModel sharedDeviceListArchiveModel] setReceivedSeviceArray:deviceArray];
    //Returns an array of device lists after comparison
    NSMutableArray *array = [[DevicelistArchiveModel sharedDeviceListArchiveModel] devicelistCompare];
    if (array && array.count >0) {
        deviceArray = [array mutableCopy];
    }
}

@end
