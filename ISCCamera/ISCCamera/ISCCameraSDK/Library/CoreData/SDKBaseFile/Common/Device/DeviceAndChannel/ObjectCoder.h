//
//  ObjectCoder.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjectCoder : NSObject<NSCoding, NSCopying, NSMutableCopying>
@end
