//
//  ChannelObject.m
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "ChannelObject.h"

@implementation ChannelObject

- (instancetype)init {
    self = [super init];
    if (self) {
        _deviceMac = @"";
        _channelName = @"";
        _loginName = @"admin";
        _loginPsw = @"";
        _channelNumber = 0;
    }
    return self;
}
@end
