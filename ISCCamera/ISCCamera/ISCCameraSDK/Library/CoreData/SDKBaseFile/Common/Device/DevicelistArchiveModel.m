//
//  DevicelistArchiveModel.m
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "DevicelistArchiveModel.h"
#import "DeviceObject.h"

@implementation DevicelistArchiveModel
{
    NSMutableArray *savedArray;
    NSMutableArray *deviceArray;
}

+ (instancetype)sharedDeviceListArchiveModel {
    static DevicelistArchiveModel *sharedDeviceListModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDeviceListModel = [[DevicelistArchiveModel alloc] init];
    });
    return sharedDeviceListModel;
}

- (instancetype)init {
    self = [super init];
    return self;
}

- (void)setReceivedSeviceArray:(NSMutableArray*)array {
    deviceArray = [array mutableCopy];
}

#pragma mark - Get a list of devices saved locally
- (void)getSavedDeviceList:(NSString*)userName {
    //Local device list read area and processing
    if (userName == nil) {
        userName = @"local";
    }
    self.userName = userName;
    //1. Read the list of locally saved devices, which should be read when logging in.
    NSData *myEncodedObject = [[NSUserDefaults standardUserDefaults] objectForKey:userName];
    @try{
        savedArray = [NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
    if (savedArray == nil) {
        savedArray = [[NSMutableArray alloc] initWithCapacity:0];
    }
}

#pragma mark - Save device list data to local
- (void)saveDevicelist:(NSMutableArray *)deviceArray {
    if (deviceArray == nil || self.userName == nil || [self.userName isEqualToString:@""]) {
        return;
    }
    NSMutableArray *copyArray = [[NSMutableArray alloc]initWithArray:deviceArray copyItems:YES];//Deep copy array file
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (DeviceObject *deviceObj in copyArray) {//Restore some parameters of the data to be saved to default
            for (ChannelObject *channel in deviceObj.channelArray) {
            }
        }
        NSData *archiveCarPriceData = [NSKeyedArchiver archivedDataWithRootObject:copyArray];
        if (archiveCarPriceData != nil) {
            [[NSUserDefaults standardUserDefaults] setObject:archiveCarPriceData forKey:self.userName];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    });
}
#pragma mark - Determine whether the list of locally read devices is consistent with the obtained device list.
- (NSMutableArray *)devicelistCompare {
    if (deviceArray == nil || deviceArray.count == 0 || savedArray.count == 0) {
        //The data source is not complete, no comparison, direct return
        return deviceArray;
    }
    //Copy out a modified array for adding and deleting data directly during the comparison process
    NSMutableArray *amendArray = [savedArray mutableCopy];
    //Extract the local device serial number array and the obtained device serial number array separately
    NSMutableArray *tmpArray = [[NSMutableArray alloc] initWithCapacity:0];
    NSMutableArray *tmpSaveArray = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i =0; i< savedArray.count; i++) {
        DeviceObject *devObject = [savedArray objectAtIndex:i];
        [tmpSaveArray addObject:devObject.deviceMac];
    }
    for (int i =0; i< deviceArray.count; i++) {
        DeviceObject *devObject = [deviceArray objectAtIndex:i];
        [tmpArray addObject:devObject.deviceMac];
    }
    for (NSString *str in tmpSaveArray) {//Devices that have been deleted elsewhere need to be removed from the local
        if (![tmpArray containsObject:str]) {
            [amendArray removeObject:[self GetDeviceObjectBySN:str]];
        }
    }
    for (NSString *str in tmpArray) {//Devices added from other places need to be added locally
        if (![tmpSaveArray containsObject:str]) {
            [amendArray addObject:[deviceArray objectAtIndex:[tmpArray indexOfObject:str]]];
        }
    }
    //After the data comparison is complete, copy the modified array to the local array.
    return [amendArray mutableCopy];
}

#pragma mark - Get the deviceObject object by serial number
- (DeviceObject *)GetDeviceObjectBySN:(NSString *)devSN {
    if (savedArray == nil || devSN == nil) {
        return nil;
    }
    for (int i = 0; i < savedArray.count; i ++) {
        DeviceObject *devObject = [savedArray objectAtIndex:i];
        if (devObject == nil) {
            continue;
        }
        if ([devObject.deviceMac isEqualToString:devSN]) {
            return devObject;
        }
    }
    return nil;
}
@end
