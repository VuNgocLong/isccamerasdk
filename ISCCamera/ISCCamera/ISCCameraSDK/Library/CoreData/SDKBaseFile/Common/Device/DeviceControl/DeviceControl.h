//
//  DeviceControl.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

/***
 
Memory controller for device and channel information, just control memory information
 
 *****/

#import <Foundation/Foundation.h>
#import "DeviceObject.h"

@interface DeviceControl : NSObject
+ (instancetype)getInstance;

#pragma mark - Empty all cached devices
- (void)clearDeviceArray;

#pragma mark - Add device
- (void)addDevice:(DeviceObject *)devObject;

#pragma mark - Get all devices
- (NSMutableArray *)currentDeviceArray;

#pragma mark - Comparison between the device obtained by the server and the locally cached device
- (void)checkDeviceValid;

#pragma mark - Save device to local storage
- (void)saveDeviceList;

#pragma mark - Get the deviceObject object by serial number
- (DeviceObject *)GetDeviceObjectBySN:(NSString *)devSN;
#pragma mark - Initialize the channelObject object
- (ChannelObject *)addName:(NSString*)channelName ToDeviceObject:(DeviceObject*)devObject;

#pragma mark - Processing device and channel information to be played
- (void)setPlayItem:(ChannelObject *)channel;
- (NSMutableArray *)getPlayItem;
- (void)cleanPlayitem;

#pragma mark - Set the channel currently being processed, such as the channel in the capture video, the channel in the device configuration, etc. (default is 0 for single-screen preview)
- (void)setSelectChannel:(NSInteger)selectChannel;
- (NSInteger)getSelectChannelIndex;
- (ChannelObject*)getSelectChannel;
@end
