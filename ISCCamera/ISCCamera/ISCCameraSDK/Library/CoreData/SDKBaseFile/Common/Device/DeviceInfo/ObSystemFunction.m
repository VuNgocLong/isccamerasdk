//
//  ObSystemFunction.m
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "ObSystemFunction.h"

@implementation ObSystemFunction

- (instancetype)init {
    self = [super init];
    if (self) {
        _deviceMac = @"";
        _channelNumber = 0;
        _NewVideoAnalyze = NO;
        _SupportIntelligentPlayBack = NO;
        _SupportSetDigIP = NO;
        _IPConsumer433Alarm = NO;
        _SupportSmartH264 = NO;
    }
    return self;
}
@end
