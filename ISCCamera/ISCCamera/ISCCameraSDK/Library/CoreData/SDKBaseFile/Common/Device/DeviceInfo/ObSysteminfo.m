//
//  ObSysteminfo.m
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

#import "ObSysteminfo.h"

@implementation ObSysteminfo
- (instancetype)init {
    self = [super init];
    if (self) {
        _deviceMac = @"";
        _channelNumber = 0;
        _type = -1;
        _eFunDevState = 0;
        _SerialNo = @"";
        _buildTime = @"";
        _softWareVersion = @"";
        _hardWare = @"";
        _netType =NetTypeNone;
    }
    return self;
}
@end
