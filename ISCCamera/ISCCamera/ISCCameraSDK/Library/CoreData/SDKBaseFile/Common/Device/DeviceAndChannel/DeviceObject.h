//
//  DeviceObject.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

/***
 
 Device object class
 
 *****/
#import "ObjectCoder.h"
#import "ChannelObject.h"

#import "ObSysteminfo.h"
#import "ObSystemFunction.h"

@interface DeviceObject : ObjectCoder

@property (nonatomic, copy) NSString *deviceMac;
@property (nonatomic, copy) NSString *deviceName;
@property (nonatomic, copy) NSString *loginName;
@property (nonatomic, copy) NSString *loginPsw;
@property (nonatomic, copy) NSString *deviceIp;                          //Device IP

@property (nonatomic) int state;     //online status
@property (nonatomic) int nPort;     //The port number
@property (nonatomic) int nType;     //Equipment type
@property (nonatomic) int nID;      //Expansion

@property (nonatomic, strong) ObSysteminfo *info; //Device Information
@property (nonatomic, strong) ObSystemFunction *sysFunction; //Equipment capability level

@property (nonatomic, strong) NSMutableArray *channelArray; //Channel array

@end
