//
//  ObSystemFunction.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

/***
 
Equipment capability level
 
 *****/

#import "ObjectCoder.h"

@interface ObSystemFunction : ObjectCoder

@property (nonatomic, copy) NSString *deviceMac;
@property (nonatomic) int channelNumber;

@property (nonatomic) BOOL NewVideoAnalyze;//Whether to support intelligent analysis alarm
@property (nonatomic) BOOL SupportIntelligentPlayBack;//Whether to support smart fast release
@property (nonatomic) BOOL SupportSetDigIP;//Whether to modify the front-end IP
@property (nonatomic) BOOL IPConsumer433Alarm;//Whether to support 433 alarm
@property (nonatomic) BOOL SupportSmartH264;//Whether to support h264+

@end
