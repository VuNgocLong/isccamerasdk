//
//  ChannelObject.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

/***
 
 Device channel information
 
 *****/

#import "ObjectCoder.h"

@interface ChannelObject : ObjectCoder

@property (nonatomic, copy) NSString *deviceMac;
@property (nonatomic, copy) NSString *channelName;
@property (nonatomic, copy) NSString *loginName;
@property (nonatomic, copy) NSString *loginPsw;
@property (nonatomic) int channelNumber;  //Channel number
@property (nonatomic) BOOL isFish;  //Is it a panoramic correction?
@end
