//
//  ObSysteminfo.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

/***
 
Device information class
 
 *****/
typedef enum NetTypeModel {
    
    NetTypeNone = -1,
    NetTypeP2P_Mode = 0,//P2P
    NetTypeTransmit_Mode,//Forward
    NetTypeIP_Mode,//IP
    NetTypeRPS_Mode = 5, //RPS
    NetTypeRTS_P2P,
    NetTypeRTS_Proxy,
    NetTypeP2P_V2,
    NetTypeProxy_V2,
    
}NetTypeModel;

#import "ObjectCoder.h"

@interface ObSysteminfo : ObjectCoder

@property (nonatomic, copy) NSString *deviceMac;
@property (nonatomic) int channelNumber;

@property (nonatomic, assign) int  type;                    //Equipment type
@property (nonatomic, assign) int eFunDevState;             //Other device status EFunDevState 0 Unknown 1 Wake up 2 Sleep 3 Cannot wake up sleep 4 Ready to sleep

@property (nonatomic, copy) NSString *SerialNo;
@property (nonatomic, copy) NSString *buildTime;
@property (nonatomic, copy) NSString *softWareVersion;
@property (nonatomic, copy) NSString *hardWare;
@property (nonatomic) enum NetTypeModel netType;
@property (nonatomic) int  nVideoInChanNum;      //The number of analog channels, more than the digital channel
@end
