//
//  DevicelistArchiveModel.h
//  camera
//
//  Created by Phan Hai Son on 2/17/19.
//  Copyright © 2019 IoT FTEL. All rights reserved.
//

/******
 *Device information local save and read class
 *If you need to save the device information locally, you can call save after obtaining the device information, read it from the list of server devices, and then compare
 *
 *******/

#import <Foundation/Foundation.h>

@interface DevicelistArchiveModel : NSObject

@property (nonatomic, copy) NSString *userName;

+ (instancetype)sharedDeviceListArchiveModel;

#pragma mark - Get a list of devices saved locally
- (void)getSavedDeviceList:(NSString *)userName;

#pragma mark - Save device list data to local
- (void)saveDevicelist:(NSMutableArray *)deviceArray;

#pragma mark - Pass the list of devices currently obtained from the server
- (void)setReceivedSeviceArray:(NSMutableArray *)array;

#pragma mark - Determine whether the list of locally read devices is consistent with the obtained device list.
- (NSMutableArray *)devicelistCompare;
@end
